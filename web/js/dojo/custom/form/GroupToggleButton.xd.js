/*
	Copyright (c) 2004-2011, The Dojo Foundation All Rights Reserved.
	Available via Academic Free License >= 2.1 OR the modified BSD license.
	see: http://dojotoolkit.org/license for details
*/


dojo._xdResourceLoaded(function(dojo, dijit, dojox){
return {depends: [["provide", "custom.form.GroupToggleButton"],
["require", "dijit.form.ToggleButton"]],
defineResource: function(dojo, dijit, dojox){if(!dojo._hasResource["custom.form.GroupToggleButton"]){ //_hasResource checks added by build. Do not use _hasResource directly in your code.
dojo._hasResource["custom.form.GroupToggleButton"] = true;
/**
 * User: gabe
 * Date: 6/23/11
 * Time: 9:53 PM
 */

dojo.provide("custom.form.GroupToggleButton");

dojo.require("dijit.form.ToggleButton");

dojo.declare("custom.form.GroupToggleButton", dijit.form.ToggleButton,
{
	// summary:
	//		A toggle button that is a member of a group where only one may be selected.
	//
	// description:
	//
	// example:
	// |	var gtb = new custom.form.GroupToggleButton({ groupName: "someGroup"});
	//

	groupName: 'defaultGroup',
	postMixInProperties:function(){
		this.inherited(arguments);
		this.unselectChannel = '/ButtonGroup/' + this.groupName;
		dojo.subscribe(this.unselectChannel, this, 'doUnselect');
	},


	/**
	 * Another button was selected. If I am selected, deselect.
	 * @param {Object} button The button that was selected.
	 */
	doUnselect: function(/*Object*/button) {
		if (button !== this && this.checked) {
			this.set('checked', false);
		}
	},

	_onClick: function(e) {
		if (this.disabled) {
			return false;
		}
		if (!this.checked) {
			this._clicked(); // widget click actions
			return this.onClick(e); // user click actions
		}
		return false;
	},

	_clicked: function() {
		dojo.publish(this.unselectChannel, [this]);
		this.set('checked', true);
	}
});

}

}};});