<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>
<!-- Not used unless you declare a <form-login login-page="/login.jsp"/> element -->

<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>Вход в систему</title>
  </head>

  <body onload="document.f.j_username.focus();">
    <h1>Вход в систему</h1>

    <%-- this form-login-page form is also used as the
         form-error-page to ask for a login again.
         --%>
	<!-- p>I want the error message to appear below when there is an error</p -->
     <c:if test="${not empty param.login_error}">
       <font color="red">
         Неудачная попытка входа<br/><br/>
         причина: <c:out value="${SPRING_SECURITY_LAST_EXCEPTION.message}"/>.
       </font>
     </c:if>
	<!-- p>I want the error message to appear above when there is an error</p -->

    <form name="f" action="<c:url value='j_spring_security_check'/>?${_csrf.parameterName}=${_csrf.token}" method="POST">
    <%--<form name="f" action="<c:url value='/login'/>" method="POST">--%>
      <table>
        <tr><td>Имя пользователя:</td><td><input type='text' name='j_username' /></td></tr>
        <tr><td>Пароль:</td><td><input type='password' name='j_password'></td></tr>
        <tr><td><input type="checkbox" name="_spring_security_remember_me"></td><td>Не просить меня ввести пароль в течение двух недель</td></tr>

        <tr><td colspan='2'><input name="submit" type="submit"></td></tr>
        <tr><td colspan='2'><input name="reset" type="reset"></td></tr>
      </table>

    </form>

  </body>
</html>
