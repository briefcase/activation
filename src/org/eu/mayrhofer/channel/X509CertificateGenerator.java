/* Copyright Rene Mayrhofer, 2006-03-19
 * 
 * This file may be copied under the terms of the GNU GPL version 2.
 */ 

package org.eu.mayrhofer.channel;

import org.apache.log4j.Logger;
import org.bouncycastle.asn1.*;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x509.*;
import org.bouncycastle.crypto.*;
import org.bouncycastle.crypto.digests.SHA1Digest;
import org.bouncycastle.crypto.encodings.PKCS1Encoding;
import org.bouncycastle.crypto.engines.RSAEngine;
import org.bouncycastle.crypto.generators.RSAKeyPairGenerator;
import org.bouncycastle.crypto.params.RSAKeyGenerationParameters;
import org.bouncycastle.crypto.params.RSAKeyParameters;
import org.bouncycastle.crypto.params.RSAPrivateCrtKeyParameters;
import org.bouncycastle.jce.PrincipalUtil;
import org.bouncycastle.jce.interfaces.PKCS12BagAttributeCarrier;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.jce.provider.X509CertificateObject;
import org.bouncycastle.openssl.PEMWriter;
import org.bouncycastle.x509.X509V2CRLGenerator;
import org.bouncycastle.x509.extension.AuthorityKeyIdentifierStructure;
import org.bouncycastle.x509.extension.SubjectKeyIdentifierStructure;

import java.io.*;
import java.math.BigInteger;
import java.security.*;
import java.security.cert.*;
import java.security.cert.CRLReason;
import java.security.cert.Certificate;
import java.security.interfaces.RSAPrivateCrtKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPrivateCrtKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.util.Calendar;
import java.util.Date;

/** This class uses the Bouncycastle lightweight API to generate X.509 certificates programmatically.
 * It assumes a CA certificate and its private key to be available and can sign the new certificate with
 * this CA. Some of the code for this class was taken from 
 * org.bouncycastle.x509.X509V3CertificateGenerator, but adapted to work with the lightweight API instead of
 * JCE (which is usually not available on MIDP2.0). 
 * 
 * @author Rene Mayrhofer
 */
public class X509CertificateGenerator {

    Logger log = Logger.getLogger(X509CertificateGenerator.class);

	/** This holds the certificate of the CA used to sign the new certificate. The object is created in the constructor. */
	private X509Certificate caCert;
    private X509CRL crl;
	/** This holds the private key of the CA used to sign the new certificate. The object is created in the constructor. */
	private RSAPrivateCrtKeyParameters caPrivateKey;
    private PrivateKey caPrivKey;
	
	private final boolean useBCAPI;

    public class CertData implements Serializable {
        public X509Certificate cert;
        public PublicKey publicKey;
        public PrivateKey privateKey;

        public CertData(X509Certificate cert, PublicKey publicKey, PrivateKey privateKey) {
            this.cert = cert;
            this.publicKey = publicKey;
            this.privateKey = privateKey;
        }
    }

    public X509Certificate getCaCert() {
        return caCert;
    }

    public X509CRL getCrl() {
        return crl;
    }

    public RSAPrivateCrtKeyParameters getCaPrivateKey() {
        return caPrivateKey;
    }

    public PrivateKey getCaPrivKey() {
        return caPrivKey;
    }

    public X509CertificateGenerator(String caFile, String caPassword, String caAlias, String crlFile, boolean useBCAPI)
			throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException, UnrecoverableKeyException, InvalidKeyException, NoSuchProviderException, SignatureException, CRLException {
        Security.addProvider(new BouncyCastleProvider());
		this.useBCAPI = useBCAPI;
		this.crl = null;

		KeyStore caKs = KeyStore.getInstance("PKCS12");
		caKs.load(new FileInputStream(new File(caFile)), caPassword.toCharArray());

		// load the key entry from the keystore
		Key key = caKs.getKey(caAlias, caPassword.toCharArray());
		if (key == null) {
			throw new RuntimeException("Got null key from keystore!");
		}
		caPrivKey = (PrivateKey) key;
        RSAPrivateCrtKey pk = (RSAPrivateCrtKey) key;
		caPrivateKey = new RSAPrivateCrtKeyParameters(pk.getModulus(), pk.getPublicExponent(), pk.getPrivateExponent(),
				pk.getPrimeP(), pk.getPrimeQ(), pk.getPrimeExponentP(), pk.getPrimeExponentQ(), pk.getCrtCoefficient());
		// and get the certificate
		caCert = (X509Certificate) caKs.getCertificate(caAlias);
		if (caCert == null) {
			throw new RuntimeException("Got null cert from keystore!"); 
		}
		caCert.verify(caCert.getPublicKey());

       if (crlFile != null) {
           try {
               InputStream inStream = new FileInputStream(crlFile);
               CertificateFactory cf = CertificateFactory.getInstance("X.509");
               crl = (X509CRL)cf.generateCRL(inStream);
           } catch (IOException ex) {
               log.error(ex.getMessage(), ex);
           }
       }
	}
	
	public CertData createCertificate(String dn, String email, String mac, String unit, String organization, String state, String country, String city, String version, int validityDays) throws
            IOException, InvalidKeyException, SecurityException, SignatureException, NoSuchAlgorithmException, NoSuchProviderException, CertificateException, InvalidKeySpecException, InvalidCipherTextException {
		SecureRandom sr = new SecureRandom();
		
		PublicKey pubKey;
		PrivateKey privKey;

		// generate the keypair for the new certificate
		if (useBCAPI) {
			RSAKeyPairGenerator gen = new RSAKeyPairGenerator();
			gen.init(new RSAKeyGenerationParameters(BigInteger.valueOf(3), sr, 1024, 80));
			AsymmetricCipherKeyPair keypair = gen.generateKeyPair();
			RSAKeyParameters publicKey = (RSAKeyParameters) keypair.getPublic();
			RSAPrivateCrtKeyParameters privateKey = (RSAPrivateCrtKeyParameters) keypair.getPrivate();
			// used to get proper encoding for the certificate
			new RSAPublicKeyStructure(publicKey.getModulus(), publicKey.getExponent());
			// JCE format needed for the certificate - because getEncoded() is necessary...
	        pubKey = KeyFactory.getInstance("RSA").generatePublic(
	        		new RSAPublicKeySpec(publicKey.getModulus(), publicKey.getExponent()));
            privKey = KeyFactory.getInstance("RSA").generatePrivate(
                    new RSAPublicKeySpec(privateKey.getModulus(), privateKey.getExponent()));
	        // and this one for the KeyStore
	        KeyFactory.getInstance("RSA").generatePrivate(
	        		new RSAPrivateCrtKeySpec(publicKey.getModulus(), publicKey.getExponent(),
	        				privateKey.getExponent(), privateKey.getP(), privateKey.getQ(), 
	        				privateKey.getDP(), privateKey.getDQ(), privateKey.getQInv()));
		}
		else {
			// this is the JSSE way of key generation
			KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
			keyGen.initialize(1024, sr);
			KeyPair keypair = keyGen.generateKeyPair();
			privKey = keypair.getPrivate();
			pubKey = keypair.getPublic();
		}
	    
		Calendar expiry = Calendar.getInstance();
		expiry.add(Calendar.DAY_OF_YEAR, validityDays);

//        String subject = "E=" + email + ",CN=" + dn + ",M=" + mac + ",V=" + version +
//                ",OU=" + unit + ",O=" + organization + ",ST=" + state + ",C=" + country;
        String subject = "E=" + (email == null ? "" : email.replaceAll(",", "&#44;")) + ",CN=" + (dn == null ? "" : dn.replaceAll(",", "&#44;")) + ";" + (mac == null ? "" : mac.replaceAll(",", "&#44;")) + ";" + (version == null ? "" : version) +
                ",OU=" + (unit == null ? "" : unit.replaceAll(",", "&#44;")) + ",O=" + (organization == null ? "" : organization.replaceAll(",", "&#44;")) + ",L=" + (city == null ? "" : city.replaceAll(",", "&#44;")) + ",ST=" + (state == null ? "" : state.replaceAll(",", "&#44;")) + ",C=" + (country == null ? "" : country.replaceAll(",", "&#44;"));
		X500Name x509Name = new X500Name(subject);
		V3TBSCertificateGenerator certGen = new V3TBSCertificateGenerator();
	    certGen.setSerialNumber(new DERInteger(BigInteger.valueOf(System.currentTimeMillis())));
		certGen.setIssuer(PrincipalUtil.getSubjectX509Principal(caCert));
		certGen.setSubject(x509Name);
		DERObjectIdentifier sigOID = PKCSObjectIdentifiers.md5WithRSAEncryption;
		AlgorithmIdentifier sigAlgId = new AlgorithmIdentifier(sigOID, new DERNull());
		certGen.setSignature(sigAlgId);
		certGen.setSubjectPublicKeyInfo(new SubjectPublicKeyInfo((ASN1Sequence)new ASN1InputStream(
                new ByteArrayInputStream(pubKey.getEncoded())).readObject()));

        Calendar calendar = Calendar.getInstance();
        //calendar.add(Calendar.DAY_OF_YEAR, -1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        
		certGen.setStartDate(new Time(calendar.getTime()));
		certGen.setEndDate(new Time(expiry.getTime()));
		// attention: hard coded to be SHA1+RSA!
		SHA1Digest digester = new SHA1Digest();
		AsymmetricBlockCipher rsa = new PKCS1Encoding(new RSAEngine());
		TBSCertificateStructure tbsCert = certGen.generateTBSCertificate();

		ByteArrayOutputStream   bOut = new ByteArrayOutputStream();
		DEROutputStream         dOut = new DEROutputStream(bOut);
		dOut.writeObject(tbsCert);

		// and now sign
		byte[] signature;
		if (useBCAPI) {
			byte[] certBlock = bOut.toByteArray();
			// first create digest
			digester.update(certBlock, 0, certBlock.length);
			byte[] hash = new byte[digester.getDigestSize()];
			digester.doFinal(hash, 0);
			// and sign that
			rsa.init(true, caPrivateKey);
			DigestInfo dInfo = new DigestInfo( new AlgorithmIdentifier(X509ObjectIdentifiers.id_SHA1, null), hash);
			byte[] digest = dInfo.getEncoded(ASN1Encodable.DER);
			signature = rsa.processBlock(digest, 0, digest.length);
		} else {
			// or the JCE way
            if (caPrivKey == null)
                caPrivKey = KeyFactory.getInstance("RSA").generatePrivate(
                        new RSAPrivateCrtKeySpec(caPrivateKey.getModulus(), caPrivateKey.getPublicExponent(),
                                caPrivateKey.getExponent(), caPrivateKey.getP(), caPrivateKey.getQ(),
                                caPrivateKey.getDP(), caPrivateKey.getDQ(), caPrivateKey.getQInv()));

	        Signature sig = Signature.getInstance(sigOID.getId());
	        sig.initSign(caPrivKey, sr);
	        sig.update(bOut.toByteArray());
	        signature = sig.sign();
		}

		// and finally construct the certificate structure
        ASN1EncodableVector  v = new ASN1EncodableVector();

        v.add(tbsCert);
        v.add(sigAlgId);
        v.add(new DERBitString(signature));

        X509CertificateObject clientCert = new X509CertificateObject(new X509CertificateStructure(new DERSequence(v))); 
        clientCert.verify(caCert.getPublicKey());

        PKCS12BagAttributeCarrier bagCert = clientCert;
        bagCert.setBagAttribute(PKCSObjectIdentifiers.pkcs_9_at_friendlyName,
        		new DERBMPString("Certificate for iPad cabinet"));
        bagCert.setBagAttribute(
                PKCSObjectIdentifiers.pkcs_9_at_localKeyId,
                new SubjectKeyIdentifierStructure(pubKey));

//        KeyStore store = KeyStore.getInstance("PKCS12");
//
//        store.load(null, null);
//
//        X509Certificate[] chain = new X509Certificate[2];
//        // first the client, then the CA certificate
//        chain[0] = clientCert;
//        chain[1] = caCert;
//
//        store.setKeyEntry("Private key for IPSec WLAN access", privKey, exportPassword.toCharArray(), chain);
//
//        FileOutputStream fOut = new FileOutputStream(exportFile);
//
//        store.store(fOut, exportPassword.toCharArray());
        return new CertData(clientCert, pubKey, privKey);
	}

    public void saveCertificate(CertData data, OutputStream output, String password, String alias)
            throws KeyStoreException, NoSuchAlgorithmException, IOException, CertificateException {
        KeyStore store = KeyStore.getInstance("PKCS12");
        store.load(null, null);
        X509Certificate[] chain = new X509Certificate[2];
        // first the client, then the CA certificate
        chain[0] = (X509Certificate)data.cert;
        chain[1] = caCert;
        store.setKeyEntry(alias, data.privateKey, password.toCharArray(), chain);
        store.store(output, password.toCharArray());
    }
    public CertData restoreCertificate(InputStream input, String password, String alias) throws NoSuchAlgorithmException, KeyStoreException, UnrecoverableKeyException, IOException, CertificateException {
        KeyStore store = KeyStore.getInstance("PKCS12");
        store.load(input, password.toCharArray());
        CertData data = new CertData(null, null, null);
        Certificate[] chain = store.getCertificateChain(alias);
        if (chain != null) {
            data.cert = (X509Certificate)chain[0];
        }
        data.privateKey = (PrivateKey) store.getKey(alias, password.toCharArray());
        return data;
    }

	public X509CRL generateCRL(X509Certificate clientCert) throws GeneralSecurityException {
        Date now = new Date();
        X509V2CRLGenerator   crlGen = new X509V2CRLGenerator();
        crlGen.setIssuerDN(caCert.getSubjectX500Principal());
        crlGen.setThisUpdate(now);
        crlGen.setSignatureAlgorithm("SHA1WITHRSAENCRYPTION");
        crlGen.addCRLEntry( clientCert.getSerialNumber(), now, CRLReason.PRIVILEGE_WITHDRAWN.ordinal());
        crlGen.addExtension(X509Extensions.AuthorityKeyIdentifier, false, new AuthorityKeyIdentifierStructure(caCert));
        crlGen.addExtension(X509Extensions.CRLNumber, false, new CRLNumber(clientCert.getSerialNumber()));
        if (crl != null)
            crlGen.addCRL(crl);
        crl = crlGen.generateX509CRL(caPrivKey, BouncyCastleProvider.PROVIDER_NAME);
        return crl;
    }

    public void saveCRL(String fileName) throws IOException {
        PEMWriter writer = new PEMWriter(new FileWriter(fileName));
        writer.writeObject(crl);
        writer.close();
    }
    /** The test CA can e.g. be created with
	 * 
	 * echo -e "AT\nUpper Austria\nSteyr\nMy Organization\nNetwork tests\nTest CA certificate\nme@myserver.com\n\n\n" | \
	     openssl req -new -x509 -outform PEM -newkey rsa:2048 -nodes -keyout /tmp/ca.key -keyform PEM -out /tmp/ca.crt -days 365;
	   echo "test password" | openssl pkcs12 -export -in /tmp/ca.crt -inkey /tmp/ca.key -out ca.p12 -name "Test CA" -passout stdin
	 * 
	 * The created certificate can be displayed with
	 * 
	 * openssl pkcs12 -nodes -info -in test.p12 > /tmp/test.cert && openssl x509 -noout -text -in /tmp/test.cert
	 */
	
	public static void main(String[] args) throws Exception {
        X509CertificateGenerator generator = new X509CertificateGenerator("C:\\OpenSSL-Win32\\bin\\demoCA\\ca.pfx", "123456", "1", "C:\\OpenSSL-Win32\\bin\\demoCA\\crl.crl", false);
        CertData certData = generator.createCertificate("Zhzh", "zhzh@biz-it.ru", "912319919239", "DPT", "Government", "Stavropol", "RU", "Stavropol", "1.0", 14);

        String exportFile = "C:\\OpenSSL-Win32\\bin\\demoCA\\test4.crt";
        PEMWriter pemWriter = new PEMWriter(new java.io.FileWriter(exportFile));
        pemWriter.writeObject(certData.cert);
        pemWriter.close();

//        X509CRL newCrl = generator.generateCRL(certData.cert, generator.crl);
//        exportFile = "C:\\OpenSSL-Win32\\bin\\demoCA\\crl.crl";
//
//        pemWriter = new PEMWriter(new java.io.FileWriter(exportFile));
//        pemWriter.writeObject(newCrl);
//        pemWriter.close();

        KeyStore store = KeyStore.getInstance("PKCS12");
        store.load(null, null);
        X509Certificate[] chain = new X509Certificate[2];
        // first the client, then the CA certificate
        chain[0] = (X509Certificate)certData.cert;
        chain[1] = generator.caCert;
        String exportPassword = "123456";
        exportFile = "C:\\OpenSSL-Win32\\bin\\demoCA\\test4.pfx";
        store.setKeyEntry("Private key for iPad cabinet", certData.privateKey, exportPassword.toCharArray(), chain);
        FileOutputStream fOut = new FileOutputStream(exportFile);
        store.store(fOut, exportPassword.toCharArray());

		System.out.println(certData.cert);
//        System.out.println(new X509CertificateGenerator("C:\\OpenSSL-Win32\\bin\\demoCA\\ca.pfx", "123456", "1", false).createCertificate("Roman Tkachev", "delo_roman@biz-it.ru", "", "", "Business IT OOO", "Stavropol", "RU", "Stavropol", "1.0", 3600, "C:\\OpenSSL-Win32\\bin\\demoCA\\roman.crt"));
	}
}
