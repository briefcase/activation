package ru.biz_it.cabinet.web;

import org.hibernate.validator.constraints.NotEmpty;

public class GetEncodedPasswordForm {

    @NotEmpty
    private String password;

    private String encodedPassword;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEncodedPassword() {
        return encodedPassword;
    }

    public void setEncodedPassword(String encodedPassword) {
        this.encodedPassword = encodedPassword;
    }
}
