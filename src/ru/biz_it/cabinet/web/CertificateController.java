package ru.biz_it.cabinet.web;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.biz_it.cabinet.CertificateUtils;
import ru.biz_it.cabinet.db.CertificateDAO;
import ru.biz_it.cabinet.web.shared.CertificateData;

@Controller
public class CertificateController {

    Logger log = Logger.getLogger(UserController.class);

    private CertificateDAO certDAO;
    private CertificateUtils utils;

    @Autowired
    public CertificateController(CertificateDAO certDAO, CertificateUtils utils) {
        this.certDAO = certDAO;
        this.utils = utils;
    }
    
    @RequestMapping(value = "/secure/revokeCert.json")
    public @ResponseBody ActionResult revokeCertificate(@RequestParam int id) {
        ActionResult result = new ActionResult();
        CertificateData certificateData = certDAO.read(id, null);
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (!user.getAuthorities().contains(new SimpleAuthority("supervisor")) &&
                !user.getUsername().equals(certificateData.getPrincipalName())) {
            result.setResult("AccessDenied");
        }
        if (result.getResult() == null) {
            certificateData.setInactive(true);
            try {
                utils.revokeCertificate(certificateData);
                certDAO.save(certificateData);
                result.setResult("OK");
            } catch (Exception ex) {
                log.error(ex.getMessage(), ex);
                result.setResult("Error");
            }
        }
        return result;
    }

    @RequestMapping(value = "/secure/reissueCert.json")
    public @ResponseBody CertificateData reissueCertificate(@RequestParam int id) {
        CertificateData certificateData = certDAO.read(id, null);
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (!user.getAuthorities().contains(new SimpleAuthority("supervisor")) &&
                !user.getUsername().equals(certificateData.getPrincipalName())) {
            return null;
        }

        certificateData.setInactive(true);
        try {
            utils.revokeCertificate(certificateData);
            certDAO.save(certificateData);
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            return null;
        }
        try {
            CertificateData newCert = utils.createCertificate(certificateData.getUserData(), null, 
                    utils.getNewCertificateValidity(certificateData), certificateData);
            certDAO.save(newCert);
            return newCert;
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            return null;
        }
    }

    @RequestMapping(value = "/secure/deleteCert.json")
    public @ResponseBody ActionResult deleteCertificate(@RequestParam int id) {
        ActionResult result = new ActionResult();
        CertificateData certificateData = certDAO.read(id, null);
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (!user.getAuthorities().contains(new SimpleAuthority("supervisor")) &&
                !user.getUsername().equals(certificateData.getPrincipalName())) {
            result.setResult("AccessDenied");
        }
        if (result.getResult() == null) {
            certificateData.setInactive(true);
            try {
                utils.revokeCertificate(certificateData);
                certDAO.delete(certificateData);
                result.setResult("OK");
            } catch (Exception ex) {
                log.error(ex.getMessage(), ex);
                result.setResult("Error");
            }
        }
        return result;
    }
}
