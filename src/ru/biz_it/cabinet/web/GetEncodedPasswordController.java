package ru.biz_it.cabinet.web;


import org.springframework.security.crypto.password.StandardPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ru.biz_it.cabinet.web.GetEncodedPasswordForm;

import javax.validation.Valid;

@Controller
@RequestMapping(value = {"/secure/getEncodedPassword.go", "/secure/getEncodedPassword.json"})
public class GetEncodedPasswordController {

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView onGet() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("getEncodedPasswordForm");
        mav.addObject(new GetEncodedPasswordForm());
        return mav;
    }

    @RequestMapping(method = RequestMethod.POST)
    public ModelAndView onSubmit(@Valid GetEncodedPasswordForm getEncodedPasswordForm, BindingResult result) {
        if (result.hasErrors()) {
            ModelAndView mav = new ModelAndView();
            mav.setViewName("getEncodedPasswordForm");
            return mav;
        }
        getEncodedPasswordForm.setEncodedPassword(new StandardPasswordEncoder().encode(getEncodedPasswordForm.getPassword()));
        ModelAndView mav = new ModelAndView();
        mav.setViewName("getEncodedPassword");
        mav.addObject(getEncodedPasswordForm);
        return mav;
    }



}
