package ru.biz_it.cabinet.web;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.biz_it.cabinet.db.CertificateDAO;
import ru.biz_it.cabinet.db.UserDAO;
import ru.biz_it.cabinet.web.shared.*;

import java.util.List;

@Controller
public class UserController {

    Logger log = Logger.getLogger(UserController.class);
    
    private UserDAO userDAO;
    private CertificateDAO certDAO;

    @Autowired
    public UserController(UserDAO userDAO, CertificateDAO certDAO) {
        this.userDAO = userDAO;
        this.certDAO = certDAO;
    }

    @RequestMapping (value = "/secure/deleteUser.json")
    public @ResponseBody ActionResult deleteUser(@RequestParam int id) {
        ActionResult result = new ActionResult();
        UserData userData = userDAO.read(id, null);
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (!user.getAuthorities().contains(new SimpleAuthority("supervisor")) &&
                !user.getUsername().equals(userData.getPrincipalName())) {
            result.setResult("AccessDenied");
        } else {
            ListBean listBean = new CertificateListBean();
            listBean.addSelection("userId", id);
            List<DataBean> certs = certDAO.readList(listBean);
            for (DataBean cert : certs) {
                certDAO.delete(cert);
            }
            userDAO.delete(userData);
            result.setResult("OK");
        }
        return result;
    }

}
