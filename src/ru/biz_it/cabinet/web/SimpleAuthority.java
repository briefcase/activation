package ru.biz_it.cabinet.web;

import org.springframework.security.core.GrantedAuthority;

public class SimpleAuthority implements GrantedAuthority {
    private String authorityName;

    public SimpleAuthority(String authorityName) {
        this.authorityName = authorityName;
    }

    @Override
    public String getAuthority() {
        return authorityName;
    }
}
