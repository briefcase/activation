package ru.biz_it.cabinet.web;

import ru.biz_it.cabinet.web.shared.DataBean;

public class ActionResult{

    String result;
    DataBean object;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public DataBean getObject() {
        return object;
    }

    public void setObject(DataBean object) {
        this.object = object;
    }
}
