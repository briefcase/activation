package ru.biz_it.cabinet.web.shared;

public abstract class DataBean implements IDataObject {

    public DataBean() {
    }

    private String principalName;

    public String getPrincipalName() {
        return principalName;
    }

    public void setPrincipalName(String principalName) {
        this.principalName = principalName;
    }
}
