package ru.biz_it.cabinet.web.shared;

public interface IDataObject {
    int getId();
    void setId(int id);
}
