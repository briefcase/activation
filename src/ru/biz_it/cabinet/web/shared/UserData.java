package ru.biz_it.cabinet.web.shared;

import java.io.Serializable;
import java.util.*;

public class UserData extends DataBean implements Serializable {

    public UserData() {
    }

    private int id;
    private String macAddress;
    private String inn;
    private String name;
    private String email;
    private String orgName;
    private String orgTitle;
    private String region;
    private String country;
    private String city;
    private int orgId;
    private Organization organization;

    private List<CertificateData> userCertificates;
    private boolean changeMac;

    public UserData(int id) {
        this.id = id;
    }

    public UserData(String macAddress) {
        this.macAddress = macAddress;
    }

    public int getOrgId() {
        return orgId;
    }

    public void setOrgId(int orgId) {
        this.orgId = orgId;
    }

    public List<CertificateData> getUserCertificates() {
        return userCertificates;
    }

    public void setUserCertificates(List<CertificateData> userCertificates) {
        this.userCertificates = userCertificates;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getOrgTitle() {
        return orgTitle;
    }

    public void setOrgTitle(String orgTitle) {
        this.orgTitle = orgTitle;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getInn() {
        return inn;
    }

    public void setInn(String inn) {
        this.inn = inn;
    }

    public boolean isChangeMac() {
        return changeMac;
    }

    public void setChangeMac(boolean changeMac) {
        this.changeMac = changeMac;
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
        this.orgId = organization.getId();
        this.orgName = organization.getOrgName();
    }

    public void copyOrganizationFields() {
        this.setOrgName(organization.getOrgName());
        this.setPrincipalName(organization.getPrincipalName());
        this.setCity(organization.getCity());
        this.setCountry(organization.getCountry());
        this.setInn(organization.getInn());
        this.setRegion(organization.getRegion());
    }
}
