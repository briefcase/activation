package ru.biz_it.cabinet.web.shared;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public abstract class ListBean implements Serializable {
    
    private int count;
    private int start;
    private HashMap<String, Object> selection;
    private HashMap<String, String> selectionOperations;
    private List<String> order;
    private boolean desc;
    private String defaultOrder;
    private boolean activeOnly;

    protected ListBean() {
        this.selection = new HashMap<>();
        this.selectionOperations = new HashMap<>();
        order = new ArrayList<>();
    }


    public boolean isActiveOnly() {
        return activeOnly;
    }

    public void setActiveOnly(boolean activeOnly) {
        this.activeOnly = activeOnly;
    }


    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public HashMap<String, Object> getSelection() {
        return selection;
    }

    public void setSelection(HashMap<String, Object> selection) {
        this.selection = selection;
    }

    public HashMap<String, String> getSelectionOperations() {
        return selectionOperations;
    }

    public void setSelectionOperations(HashMap<String, String> selectionOperations) {
        this.selectionOperations = selectionOperations;
    }

    public List<String> getOrder() {
        return order;
    }

    public void setOrder(List<String> order) {
        this.order = order;
    }

    public boolean isDesc() {
        return desc;
    }

    public void setDesc(boolean desc) {
        this.desc = desc;
    }
    
    public void addSelection(String columnName, Object value) {
        if (selection == null) {
            selection = new HashMap<>();
        }
        selection.put(columnName, value);
        
    }

    public void addSelectionOperation(String columnName, String operation) {
        if (selectionOperations == null) {
            selectionOperations = new HashMap<>();
        }
        selectionOperations.put(columnName, operation);

    }


    public void addOrder(String columnName) {
        order.add(columnName);
    }

    public void removeSelection(String columnName, Object value) {
        if (selection == null) {
            selection = new HashMap<>();
        }
        selection.remove(columnName);

    }

    public void removeSelectionOperation(String columnName) {
        if (selectionOperations == null) {
            selectionOperations = new HashMap<>();
        }
        selectionOperations.remove(columnName);

    }

    public void removeOrder(String columnName) {
        order.remove(columnName);
    }

    public abstract String getListActionName();

    public String getDefaultOrder() {
        return defaultOrder;
    }

    public void setDefaultOrder(String defaultOrder) {
        this.defaultOrder = defaultOrder;
    }
}
