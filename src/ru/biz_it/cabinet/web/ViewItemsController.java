package ru.biz_it.cabinet.web;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContextUtils;
import ru.biz_it.cabinet.CertificateUtils;
import ru.biz_it.cabinet.db.*;
import ru.biz_it.cabinet.web.shared.*;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.springframework.security.core.context.SecurityContextHolder.getContext;

@Controller
public class ViewItemsController {

    private Logger log = Logger.getLogger(ViewItemsController.class);

    private CertificateDAO certDAO;
    private RegistrationKeyDAO keyDAO;
    private UserDAO userDAO;
    private OrganizationDAO orgDAO;
    private CertificateUtils utils;

    private ServletContext servletContext;

    public ViewItemsController() {}

    @Autowired
    public ViewItemsController(CertificateDAO certDAO, RegistrationKeyDAO keyDAO, UserDAO userDAO, CertificateUtils utils, OrganizationDAO orgDAO) {
        this.certDAO = certDAO;
        this.keyDAO = keyDAO;
        this.userDAO = userDAO;
        this.utils = utils;
        this.orgDAO = orgDAO;
    }

    private void fillListBean(ListBean listBean, WebRequest request, User user, HashMap<String, String> defaultOperationMap, HashMap<String, Object> params) {
        Iterator<String> names = params == null ? request.getParameterNames() : params.keySet().iterator();
        for (; names.hasNext();  ) {
            String paramName = names.next();
            Object paramValue = params == null ? request.getParameter(paramName) : params.get(paramName);
            if (paramValue != null) {
                if (paramName.startsWith("order")) {
                    String orderField = paramName.substring(5);
                    if("".equals(paramValue)) {
                        listBean.removeOrder(orderField);
                    } else if(!listBean.getOrder().contains(orderField)) {
                        listBean.getOrder().clear();
                        listBean.addOrder(orderField);
                    }
                } else if ("count".equals(paramName)) {
                    listBean.setCount(Integer.parseInt((String)paramValue));
                    if (listBean.getCount() < 0) {
                        listBean.setCount(0);
                    }
                } else if ("start".equals(paramName)) {
                    listBean.setStart(Integer.parseInt((String)paramValue));
                } else if ("desc".equals(paramName)) {
                    listBean.setDesc(Boolean.parseBoolean((String)paramValue));
                } else if ("activeOnly".equals(paramName)) {
                    if (listBean instanceof UserListBean) {
                        listBean.setActiveOnly(Boolean.parseBoolean((String) paramValue));
                    }
                } else {
                    if("".equals(paramValue)) {
                        listBean.removeSelection(paramName, paramValue);
                    } else {
                        listBean.addSelection(paramName, paramValue);
                        if (defaultOperationMap != null && defaultOperationMap.containsKey(paramName)) {
                            listBean.addSelectionOperation(paramName, defaultOperationMap.get(paramName));
                        }
                    }

                }
            }
            addPrincipalSelection(listBean, user);
        }
        if (listBean.getOrder().size() == 0 && !(listBean.getDefaultOrder() == null || "".equals(listBean.getDefaultOrder()))) {
            listBean.addOrder(listBean.getDefaultOrder());
        }
    }

    private void addPrincipalSelection(ListBean listBean, User user) {
        if (!user.getAuthorities().contains(new SimpleAuthority("supervisor"))) {
            String userName = (String)listBean.getSelection().get("principalName");
            if (userName == null || "".equals(userName) || !user.getUsername().equals(userName))
                listBean.addSelection("principalName", user.getUsername());
        }
    }

    @RequestMapping(value = {"/secure/viewCertificate.go"})
    public ModelAndView viewCertificate(@RequestParam int id) throws ServletException {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("certificateForm");
        CertificateData certificateData = getCertificate(id);
        mav.addObject(certificateData);
        return mav;
    }

    private DataBean getDataBean(CabinetDAO daoObject, int id) {
        try {
            User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            if (user.getAuthorities().contains(new SimpleAuthority("supervisor"))) {
                return daoObject.read(id, null);
            } else {
                return daoObject.read(id, user.getUsername());
            }
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            throw new RuntimeException(ex);
        }
    }

    @RequestMapping(value = {"/secure/viewCertificate.json"})
    public @ResponseBody CertificateData getCertificate(int id) {
        return (CertificateData) getDataBean(certDAO, id);
    }

    @RequestMapping(value = {"/secure/viewUser.go"})
    public ModelAndView viewUser(@RequestParam int id) throws ServletException {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("userForm");
        UserData userData = getUser(id);
        mav.addObject(userData);
        return mav;
    }

    @RequestMapping(value = {"/secure/viewUser.json"})
    public @ResponseBody UserData getUser(@RequestParam int id) {
        return (UserData) getDataBean(userDAO, id);
    }

    @RequestMapping(value = {"/secure/viewOrg.json"})
    public @ResponseBody Organization getOrg(@RequestParam int id) {
        return (Organization) getDataBean(orgDAO, id);
    }

    @RequestMapping(value = {"/secure/viewOrg.go"})
    public ModelAndView viewOrg(@RequestParam int id) {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("organization");
        Organization organization = getOrg(id);
        mav.addObject(organization);
        return mav;
    }

    @RequestMapping(value = {"/secure/certList.go"})
    public ModelAndView certificateList(WebRequest webRequest) throws ServletException {
        return getListModelAndVew(webRequest, CertificateListBean.class, certDAO);
    }

    @RequestMapping(value = {"/secure/keyList.go"})
    public ModelAndView keyList(WebRequest webRequest) throws ServletException {
        return getListModelAndVew(webRequest, KeyListBean.class, keyDAO);
    }

    @RequestMapping(value = {"/secure/userList.go"})
    public ModelAndView userList(WebRequest webRequest) throws ServletException {
        return getListModelAndVew(webRequest, UserListBean.class, userDAO);
    }

    @RequestMapping(value = {"/secure/orgList.go"})
    public ModelAndView orgList(WebRequest webRequest) throws ServletException {
        return getListModelAndVew(webRequest, OrganizationListBean.class, orgDAO);
    }

    @RequestMapping(value = {"/secure/certList.json"})
    public @ResponseBody List getCertificateList(WebRequest webRequest) throws ServletException {
        return getList(certDAO, getListBean(webRequest, CertificateListBean.class, null, null));
    }

    @RequestMapping(value = {"/secure/certListPost.json"})
    public @ResponseBody List getCertificateListPost(WebRequest webRequest, @RequestBody ListReq listReq) throws ServletException {

        HashMap<String, String> operations = new HashMap<>();
        operations.put("regKey", " like ");
        HashMap<String, Object> params = new HashMap<>();
        params.put("count", listReq.getCount());
        params.put("start", listReq.getStart());
        String orgId = listReq.getOrgId();
        if (orgId != null && !"".equals(orgId))
            params.put("orgId", orgId);
        String key = listReq.getKey();
        if (key != null && !"".equals(key)) {
            key = key + "%";
            params.put("regKey", key);
        }
        String userId = listReq.getUserId();
        if (userId != null && !"".equals(userId))
            params.put("userId", userId);
        ListBean bean = getListBean(webRequest, CertificateListBean.class, operations, params);
        if (orgId == null || "".equals(orgId))
            bean.removeSelection("orgId", null);
        if (key == null || "".equals(key))
            bean.removeSelection("regKey", null);
        if (userId == null || "".equals(userId))
            bean.removeSelection("userId", null);
        return getList(certDAO, bean);
    }

    @RequestMapping(value = {"/secure/keyList.json"})
    public @ResponseBody List getKeyList(WebRequest webRequest) throws ServletException {
        return getList(keyDAO, getListBean(webRequest, KeyListBean.class, null, null));
    }

    @RequestMapping(value = {"/secure/keyListPost.json"})
    public @ResponseBody List getKeyListPost(WebRequest webRequest, @RequestBody ListReq listReq) throws ServletException {
        HashMap<String, String> operations = new HashMap<>();
        operations.put("key", " like ");
        HashMap<String, Object> params = new HashMap<>();
        params.put("count", listReq.getCount());
        params.put("start", listReq.getStart());
        String orgId = listReq.getOrgId();
        if (orgId != null && !"".equals(orgId))
            params.put("orgId", orgId);
        String key = listReq.getKey();
        if (key != null && !"".equals(key)) {
            key = key + "%";
            params.put("key", key);
        }
        ListBean bean = getListBean(webRequest, KeyListBean.class, operations, params);
        if (orgId == null || "".equals(orgId))
            bean.removeSelection("orgId", null);
        if (key == null || "".equals(key))
            bean.removeSelection("key", null);
        return getList(keyDAO, bean);
    }

    @RequestMapping(value = {"/secure/userList.json"})
    public @ResponseBody List getUserList(WebRequest webRequest) throws ServletException {
        return getList(userDAO, getListBean(webRequest, UserListBean.class, null, null));
    }

    @RequestMapping(value = {"/secure/userListPost.json"})
    public @ResponseBody List getUserListPost(WebRequest webRequest, @RequestBody ListReq listReq) throws ServletException {

        HashMap<String, String> operations = new HashMap<>();
        operations.put("macAddress", " like ");
        operations.put("name", " like ");
        operations.put("email", " like ");
        HashMap<String, Object> params = new HashMap<>();
        params.put("count", listReq.getCount());
        params.put("start", listReq.getStart());
        String orgId = listReq.getOrgId();
        if (orgId != null && !"".equals(orgId))
            params.put("orgId", orgId);
        String mac = listReq.getMacAddress();
        if (mac != null && !"".equals(mac)) {
            mac = mac + "%";
            params.put("macAddress", mac);
        }
        String name = listReq.getName();
        if (name != null && !"".equals(name)) {
            name = name + "%";
            params.put("name", name);
        }
        String email = listReq.getEmail();
        if (email != null && !"".equals(email)) {
            email = mac + "%";
            params.put("email", email);
        }
        String key = listReq.getKey();
        if (key != null && !"".equals(key)) {
            params.put("key", key);
        }
        ListBean bean = getListBean(webRequest, UserListBean.class, operations, params);
        if (orgId == null || "".equals(orgId))
            bean.removeSelection("orgId", null);
        if (name == null || "".equals(name))
            bean.removeSelection("name", null);
        return getList(userDAO, bean);
    }

    @RequestMapping(value = {"/secure/orgList.json"})
    public @ResponseBody List getOrgList(WebRequest webRequest) throws ServletException {
        HashMap<String, String> operations = new HashMap<>();
        operations.put("orgName", " like ");
        return getList(orgDAO, getListBean(webRequest, OrganizationListBean.class, operations, null));
    }

    @RequestMapping(value = {"/secure/orgListPost.json"})
    public @ResponseBody List getOrgListPost(WebRequest webRequest, @RequestBody ListReq listReq) throws ServletException {
        HashMap<String, String> operations = new HashMap<>();
        operations.put("orgName", " like ");
        HashMap<String, Object> params = new HashMap<>();
        params.put("count", listReq.getCount());
        params.put("start", listReq.getStart());
        String orgName = listReq.getOrgName();
        if (orgName != null && !"".equals(orgName)) {
            orgName = "%" + orgName + "%";
            params.put("orgName", orgName);
        }
        ListBean bean = getListBean(webRequest, OrganizationListBean.class, operations, params);
        if (orgName == null || "".equals(orgName))
            bean.removeSelection("orgName", null);
        return getList(orgDAO, bean);
    }
    @RequestMapping(value = {"/orgList.json"})
    public @ResponseBody List getOrgList1(WebRequest webRequest) throws ServletException {
        return getList(orgDAO, getListBean(webRequest, OrganizationListBean.class, null, null));
    }

    @RequestMapping(value = {"/secure/orgFullList.json"})
    public @ResponseBody List getFullOrgList() throws ServletException {
        ListBean listBean = new OrganizationListBean();
        listBean.setCount(0);
        listBean.addOrder("orgName");
        return getList(orgDAO, listBean);
    }

    private ModelAndView getListModelAndVew(WebRequest webRequest, Class beanClass, CabinetDAO cabinetDAO) throws ServletException {
        ModelAndView mav = new ModelAndView();
        ListBean listBean = getListBean(webRequest, beanClass, null, null);
        List list = getList(cabinetDAO, listBean);
        mav.addObject("version", utils.getVersion());
        mav.addObject("username", ((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());

        try {
            mav.addObject("listSize", cabinetDAO.getSize(listBean));
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            throw new ServletException(ex);
        }
        mav.setViewName(listBean.getListActionName());
        mav.addObject(list);
        mav.addObject(listBean);
        return mav;
    }

    private List getList(CabinetDAO cabinetDAO, ListBean listBean) throws ServletException {
        List list;
        try {
            list = cabinetDAO.readList(listBean);
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            throw new ServletException(ex);
        }
        return list;
    }

    private ListBean getListBean(WebRequest webRequest, Class beanClass, HashMap<String, String> defaultOperationsList, HashMap<String, Object> params) {
        ListBean listBean = (ListBean) RequestContextUtils.findWebApplicationContext(((ServletWebRequest) webRequest).getRequest()).getBean(beanClass);
        fillListBean(listBean, webRequest, (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal(), defaultOperationsList, params);
        return listBean;
    }

    public ServletContext getServletContext() {
        return servletContext;
    }

    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
    }

    public CertificateDAO getCertDAO() {
        return certDAO;
    }

    public void setCertDAO(CertificateDAO certDAO) {
        this.certDAO = certDAO;
    }

    public RegistrationKeyDAO getKeyDAO() {
        return keyDAO;
    }

    public void setKeyDAO(RegistrationKeyDAO keyDAO) {
        this.keyDAO = keyDAO;
    }

    public UserDAO getUserDAO() {
        return userDAO;
    }

    public OrganizationDAO getOrgDAO() {
        return orgDAO;
    }

    public void setOrgDAO(OrganizationDAO orgDAO) {
        this.orgDAO = orgDAO;
    }

    public void setUserDAO(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    @RequestMapping (value = "/secure/updateUser.json")
    public @ResponseBody ActionResult updateUser(@RequestParam int userId, @RequestParam String fieldName, @RequestParam String fieldValue) {
        return updateDataBean(userId, fieldName, fieldValue, UserData.class, userDAO);
    }

    @RequestMapping (value = "/secure/updateOrg.json")
    public @ResponseBody ActionResult updateOrganization(@RequestParam int orgId, @RequestParam String fieldName, @RequestParam String fieldValue) {
        return updateDataBean(orgId, fieldName, fieldValue, Organization.class, orgDAO);
    }

    @RequestMapping(value = "/secure/updateCert.json")
    public @ResponseBody ActionResult updateCertificate(@RequestParam int certId, @RequestParam String fieldName, @RequestParam String fieldValue) {
        return updateDataBean(certId, fieldName, fieldValue, CertificateData.class, certDAO);
    }

    @RequestMapping(value = "/secure/saveUser.json")
    public @ResponseBody ActionResult saveUser(@RequestBody UserData userData) {
        return saveDataBean(userData, userData.getId(), userDAO);
    }

    @RequestMapping(value = "/secure/saveOrg.json")
    public @ResponseBody ActionResult saveOrg(@RequestBody Organization organization) {
        ActionResult result = saveDataBean(organization, organization.getId(), orgDAO);
        result.setObject(organization);
        return result;
    }

    @RequestMapping(value = "/secure/saveCert.json")
    public @ResponseBody ActionResult saveCertificate(@RequestBody CertificateData certData) {
        return saveDataBean(certData, certData.getId(), certDAO);
    }

    private ActionResult saveDataBean(DataBean dataBean, int id, CabinetDAO dao) {
        ActionResult result = new ActionResult();
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        DataBean storedBean = null;
        if (id != 0)
            storedBean = dao.read(id, null);
        if (storedBean == null) {
            dataBean.setPrincipalName(user.getUsername());
        } else if (!user.getAuthorities().contains(new SimpleAuthority("supervisor")) &&
                !user.getUsername().equals(storedBean.getPrincipalName())) {
            result.setResult("AccessDenied");
        } else {
            dataBean.setPrincipalName(storedBean.getPrincipalName());
        }
        if (result.getResult() == null) {
            dao.save(dataBean);
            result.setResult("OK");
        }
        return result;
    }

    private ActionResult updateDataBean(int userId, String fieldName, String fieldValue, Class beanClass, CabinetDAO dao) {
        ActionResult result = new ActionResult();
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        DataBean storedData;
        if (!user.getAuthorities().contains(new SimpleAuthority("supervisor"))) {
            storedData= dao.read(userId, user.getUsername());
        } else {
            storedData= dao.read(userId, null);
        }
        if (storedData == null)
            result.setResult("NotFound");
        if (result.getResult() == null) {
            try{
                PropertyDescriptor descriptor = new PropertyDescriptor(fieldName, beanClass);
                if ("issueDate".equals(fieldName) || "endDate".equals(fieldName)) {
                    Date date;
                    if (LocaleContextHolder.getLocale().equals(Locale.forLanguageTag("ru"))) {
                        date = (new SimpleDateFormat("dd.MM.yyyy")).parse(fieldValue);
                    } else {
                        date = (new SimpleDateFormat("MM/dd/yyyy")).parse(fieldValue);
                    }
                    descriptor.getWriteMethod().invoke(storedData, date);

                } else {
                    descriptor.getWriteMethod().invoke(storedData, fieldValue);
                }
                dao.save(storedData);
                result.setResult("OK");
            } catch (Exception ex) {
                log.error(ex.getMessage(), ex);
                result.setResult("Error");
            }
        }
        return result;
    }

    @RequestMapping(value = {"/secure/index.go"})
    public ModelAndView index(WebRequest request) throws ServletException {
        ModelAndView mav = getListModelAndVew(request, OrganizationListBean.class, orgDAO);
        Object user = getContext().getAuthentication().getPrincipal();
        if (user != null && user instanceof User) {
            if (((User)user).getAuthorities().contains(new SimpleAuthority("supervisor"))) {
                mav.addObject("isSupervisor", Boolean.TRUE);
            }
        }
        mav.addObject("version", utils.getVersion());
        mav.setViewName("index");
        ListBean listBean = new KeyListBean();
        listBean.setActiveOnly(true);
        addPrincipalSelection(listBean, (User)user);
        mav.addObject("keyCount", keyDAO.getSize(listBean));
        listBean = new CertificateListBean();
        listBean.setActiveOnly(true);
        addPrincipalSelection(listBean, (User)user);
        mav.addObject("certCount", certDAO.getSize(listBean));
        return mav;
    }


    @RequestMapping(value = "/index.go")
    public void mainPage(HttpServletResponse response) throws IOException {
        response.sendRedirect("secure/index.go");

    }

    @RequestMapping(value = "/activecerts.go")
    public @ResponseBody int activeCerts() {
        ListBean listBean = new CertificateListBean();
        listBean.setActiveOnly(true);
        return certDAO.getSize(listBean);
    }

    @RequestMapping(value = "/activekeys.go")
    public @ResponseBody int activeKeys() {
        ListBean listBean = new KeyListBean();
        listBean.setActiveOnly(true);
        return keyDAO.getSize(listBean);
    }

}