package ru.biz_it.cabinet.web;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.biz_it.cabinet.CertificateUtils;
import ru.biz_it.cabinet.KeyGenerator;
import ru.biz_it.cabinet.db.OrganizationDAO;
import ru.biz_it.cabinet.db.RegistrationKeyDAO;
import ru.biz_it.cabinet.web.shared.Organization;
import ru.biz_it.cabinet.web.shared.RegistrationKey;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
public class GenerateKeysController {

    Logger log = Logger.getLogger(GenerateKeysController.class);

    private CertificateUtils utils;
    private RegistrationKeyDAO keyDAO;
    private OrganizationDAO orgDAO;

    @Autowired
    public GenerateKeysController(CertificateUtils utils, RegistrationKeyDAO keyDAO, OrganizationDAO orgDAO) {
        this.utils = utils;
        this.keyDAO = keyDAO;
        this.orgDAO = orgDAO;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/secure/generateKeys.go")
    public ModelAndView generateKeysForm() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("generateKeysForm");
        mav.addObject(new GenerateKeysForm());
        return mav;
    }


    @RequestMapping(method = RequestMethod.POST, value = "/secure/generateKeys.go")
    public ModelAndView generateKeysForm(@Valid GenerateKeysForm generateKeysForm, BindingResult result) throws ServletException {
        if (result.hasErrors()) {
            ModelAndView mav = new ModelAndView();
            mav.setViewName("generateKeysForm");
            return mav;
        }
        List<RegistrationKey> keyList = new ArrayList<>();
        for (int i = 0; i < generateKeysForm.getCount(); i++) {
            try {
                // active logged username
                String username = ((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
                String key = KeyGenerator.generateKey();
                RegistrationKey regKey = new RegistrationKey();
                regKey.setKey(key);
                regKey.setValidDays(generateKeysForm.getDays());
                regKey.setPrincipalName(username);
                regKey.setOrgName(generateKeysForm.getOrgName());
                regKey.setVersion(utils.getVersion());
                utils.saveKey(regKey, keyDAO);
                keyList.add(regKey);
            } catch (Exception ex) {
                log.error(ex.getMessage(), ex);
                throw new ServletException(ex);
            }
        }
        ModelAndView mav = new ModelAndView();
        mav.setViewName("keyList");
        mav.addObject(keyList);
        return mav;
    }


    @RequestMapping(method = RequestMethod.POST, value = "/secure/generateKeys.json")
    public @ResponseBody List<RegistrationKey> generateKeysForm(@RequestParam int days, @RequestParam int count,
                                                                @RequestParam int orgId, @RequestParam String version) throws ServletException {
        if (count == 0 || days == 0)
            return null;

        List<RegistrationKey> keyList = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            try {
                // active logged username
                User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal(); 
                String username = user.getUsername();
                String key = KeyGenerator.generateKey();
                RegistrationKey regKey = new RegistrationKey();
                regKey.setKey(key);
                regKey.setValidDays(days);
                regKey.setPrincipalName(username);
                if (orgId != 0) {
                    Organization organization = orgDAO.read(orgId, null);
                    if (user.getAuthorities().contains(new SimpleAuthority("supervisor")) || user.getUsername().equals(organization.getPrincipalName())) {
                        regKey.setOrganization(organization);
                    }
                }
                regKey.setVersion(version == null || "".equals(version) ? utils.getVersion() : version);
                utils.saveKey(regKey, keyDAO);
                keyList.add(regKey);
            } catch (Exception ex) {
                log.error(ex.getMessage(), ex);
                throw new ServletException(ex);
            }
        }
        return keyList;
    }
    
    @InitBinder
     protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder)
        throws ServletException {
        binder.registerCustomEditor(Integer.class, new IntegerEditor());
    }

    @RequestMapping(value = "/secure/printKeys.go")
    public ModelAndView getPrintableKeyList(@RequestBody RegistrationKey[] keys) {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("printableKeyList");
        mav.addObject(keys);
        return mav;
    }
}
