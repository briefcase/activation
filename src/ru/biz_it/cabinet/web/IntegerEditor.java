package ru.biz_it.cabinet.web;

import org.apache.log4j.Logger;

import java.awt.*;
import java.beans.PropertyChangeListener;
import java.beans.PropertyEditor;

public class IntegerEditor implements PropertyEditor{

    Logger log = Logger.getLogger(IntegerEditor.class);

    Integer value;
    private Object source;
    private java.util.Vector listeners;

    public IntegerEditor () {
        setSource(this);
    }

    public IntegerEditor(Object source) {
        if (source == null) {
           throw new NullPointerException();
        }
        setSource(source);
    }

    public Object getSource() {
        return source;
    }

    /**
     * Sets the source bean.
     * <p>
     * The source bean is used as the source of events
     * for the property changes. This source should be used for information
     * purposes only and should not be modified by the PropertyEditor.
     *
     * @param source source object to be used for events
     * @since 1.5
     */
    public void setSource(Object source) {
        this.source = source;
    }

    @Override
    public void setValue(Object value) {
        this.value = (Integer)value;
    }

    @Override
    public Object getValue() {
        return value;
    }

    @Override
    public boolean isPaintable() {
        return false;
    }

    @Override
    public void paintValue(Graphics gfx, Rectangle box) {
    }

    @Override
    public String getJavaInitializationString() {
        return "???";
    }

    @Override
    public String getAsText() {
        return value == null ? "" : value.toString();
    }

    @Override
    public String[] getTags() {
        return null;
    }

    @Override
    public Component getCustomEditor() {
        return null;
    }

    @Override
    public boolean supportsCustomEditor() {
        return false;
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        if (listeners == null) {
            listeners = new java.util.Vector();
        }
        listeners.addElement(listener);

    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        if (listeners == null) {
            return;
        }
        listeners.removeElement(listener);
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        try {
            value = Integer.parseInt(text);
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            value = 0;
        }


    }

}
