package ru.biz_it.cabinet.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import ru.biz_it.cabinet.db.RegistrationKeyDAO;
import ru.biz_it.cabinet.db.UserDAO;
import ru.biz_it.cabinet.web.shared.RegistrationKey;
import ru.biz_it.cabinet.web.shared.UserData;

@Controller
public class KeyController {

    private RegistrationKeyDAO keyDAO;
    private UserDAO userDAO;

    @Autowired
    public KeyController(RegistrationKeyDAO keyDAO, UserDAO userDAO) {
        this.keyDAO = keyDAO;
        this.userDAO = userDAO;
    }

    @RequestMapping(value = {"/secure/approveKey.go"})
    public ModelAndView approveKey(@RequestParam String key) {
        ModelAndView mav = new ModelAndView();
        ActionResult result = getApprovedKey(key);
        mav.addObject(result);
        mav.setViewName("approveResult");
        return mav;
    }

    @RequestMapping(value = {"/secure/approveKey.json"})
    private @ResponseBody
    ActionResult getApprovedKey(@RequestParam String key) {
        ActionResult result = new ActionResult();
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        RegistrationKey registrationKey = keyDAO.read(key);
        checkKey(result, user, registrationKey);
        if (registrationKey != null && result.getResult() == null) {
            registrationKey.setApproved(true);
            if (registrationKey.getPrincipalName() != null && !"".equals(registrationKey.getPrincipalName()) ) {
                registrationKey.setPrincipalName(user.getUsername());
            }
            if (registrationKey.getMacAddress() != null && !"".equals(registrationKey.getMacAddress())) {
                UserData userData = userDAO.read(registrationKey.getMacAddress());
                if (userData.getPrincipalName() == null || "".equals(userData.getPrincipalName())) {
                    userData.setPrincipalName(user.getUsername());
                    userDAO.save(userData);
                }
            }
            keyDAO.save(registrationKey, false);
            result.setResult("OK");
        }
        return result;
    }

    private void checkKey(ActionResult result, User user, RegistrationKey registrationKey) {
        if (registrationKey == null) {
            result.setResult("NotFound");
        } else if (!user.getAuthorities().contains(new SimpleAuthority("supervisor"))) {
            if (registrationKey.getPrincipalName() != null && !"".equals(registrationKey.getPrincipalName()) && !user.getUsername().equals(registrationKey.getPrincipalName())) {
                result.setResult("AccessDenied");
            }
        }
    }

    @RequestMapping (value = "/secure/deleteKey.json")
    private @ResponseBody ActionResult deleteKey(@RequestParam String key) {
        ActionResult result = new ActionResult();
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        RegistrationKey registrationKey = keyDAO.read(key);
        checkKey(result, user, registrationKey);
        if (registrationKey != null && result.getResult() == null) {
            keyDAO.delete(registrationKey);
            result.setResult("OK");
        }
        return result;
    }
}
