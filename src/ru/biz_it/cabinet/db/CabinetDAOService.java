package ru.biz_it.cabinet.db;

import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import ru.biz_it.cabinet.web.shared.DataBean;

public class CabinetDAOService {

    public static void SaveToDB(DataBean dataBean, String insertStmt, String updateStmt,
                                NamedParameterJdbcTemplate jdbcTemplate, SqlParameterSource namedParameters) {
        if (dataBean.getId() == 0) {
            KeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcTemplate.update(insertStmt, namedParameters, keyHolder);
            dataBean.setId(keyHolder.getKey().intValue());
        } else {
            jdbcTemplate.update(updateStmt, namedParameters);
        }
    }
}
