package ru.biz_it.cabinet.db;

import org.apache.log4j.Logger;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import ru.biz_it.cabinet.web.shared.DataBean;
import ru.biz_it.cabinet.web.shared.IDataObject;
import ru.biz_it.cabinet.web.shared.ListBean;
import ru.biz_it.cabinet.web.shared.RegistrationKey;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

@Repository
public class RegistrationKeyDAO implements CabinetDAO {

    Logger log = Logger.getLogger(RegistrationKeyDAO.class);

    private static final String INSERT_STATEMENT = "INSERT INTO reg_keys (valid_days, cert_id, key_value, principal_name, org_name, mac, approved, version, org_id) VALUES (:validDays, :certificateId, :key, :principalName, :orgName, :macAddress, :approved, :version, :orgId)";
    private static final String UPDATE_STATEMENT = "UPDATE reg_keys SET valid_days=:validDays, cert_id=:certificateId, principal_name=:principalName, org_name=:orgName, mac=:macAddress, approved=:approved, version=:version, org_id=:orgId WHERE key_value=:key";
    private static final String SELECT_STATEMENT = "SELECT cert_id, key_value, valid_days, principal_name, org_name, mac, approved, version, org_id FROM reg_keys";
    private static final String DELETE_STATEMENT = "DELETE FROM reg_keys WHERE key_value=:key";

    private NamedParameterJdbcTemplate jdbcTemplate;

    private final class RegistrationKeyMapper implements RowMapper<RegistrationKey> {

        Map<String, String> columns = new Hashtable<String, String>();

        private RegistrationKeyMapper() {
            columns.put("certificateId", "cert_id");
            columns.put("validDays", "valid_days");
            columns.put("key", "key_value");
            columns.put("principalName", "principal_name");
            columns.put("orgName", "org_name");
            columns.put("macAddress", "mac");
            columns.put("approved", "approved");
            columns.put("version", "version");
            columns.put("orgId", "org_id");
        }

        @Override
        public RegistrationKey mapRow(ResultSet resultSet, int i) throws SQLException {
            RegistrationKey key = new RegistrationKey();
            key.setKey(resultSet.getString("key_value"));
            key.setValidDays(resultSet.getInt("valid_days"));
            key.setCertificateId(resultSet.getInt("cert_id"));
            key.setOrgName(resultSet.getString("org_name"));
            key.setPrincipalName(resultSet.getString("principal_name"));
            key.setMacAddress(resultSet.getString("mac"));
            key.setApproved(resultSet.getBoolean("approved"));
            key.setVersion(resultSet.getString("version"));
            key.setOrgId(resultSet.getInt("org_id"));
            if (key.getOrgId() != 0) {
                OrganizationDAO orgDAO = new OrganizationDAO();
                orgDAO.setJdbcTemplate(jdbcTemplate);
                key.setOrganization(orgDAO.read(key.getOrgId(), null));
            }
            return key;
        }

    }

    @Autowired
    public void setDataSource(DataSource dataSource) {
        jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public void save(RegistrationKey key, boolean newKey) {
        SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(key);
        if (newKey) {
            jdbcTemplate.update(INSERT_STATEMENT, namedParameters);
        } else {
            jdbcTemplate.update(UPDATE_STATEMENT, namedParameters);
        }
    }

    public RegistrationKey read(String key) {
        try {
            return jdbcTemplate.queryForObject(SELECT_STATEMENT + " WHERE key_value=:key", new MapSqlParameterSource("key", key), new RegistrationKeyMapper());
        } catch(EmptyResultDataAccessException ex) {
            log.error(ex.getMessage(), ex);
            return null;
        }
    }

    public List readList(ListBean listBean) {
        RegistrationKeyMapper mapper = new RegistrationKeyMapper();
        return jdbcTemplate.query(SimpleQueryBuilder.buildSelectQuery(SELECT_STATEMENT, listBean, mapper.columns, true),
                new MapSqlParameterSource(listBean == null ? null :listBean.getSelection()), mapper);

    }

    public int getSize(ListBean listBean) {
        RegistrationKeyMapper mapper = new RegistrationKeyMapper();
        String query = "SELECT COUNT(*) FROM reg_keys";
        if (listBean.isActiveOnly()) {
            listBean.addSelection("approved", 1);
            listBean.addSelection("validDays", 365);
            listBean.addSelectionOperation("validDays", ">");
        }
        return jdbcTemplate.queryForObject(SimpleQueryBuilder.buildSelectQuery(query, listBean, mapper.columns, false),
                new MapSqlParameterSource(listBean == null ? null :listBean.getSelection()), Integer.class);
    }

    @Override
    public void delete(DataBean dataBean) {
        SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(dataBean);
        jdbcTemplate.update(DELETE_STATEMENT, namedParameters);
    }

    @Override
    public DataBean read(int id, String principalName) {
        throw new RuntimeException("Not implemented");
    }

    @Override
    public void save(DataBean dataObject) {
        throw new RuntimeException("Not implemented");
    }

    @Override
    public int getCount(@Nullable String principalName) {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
