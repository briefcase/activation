package ru.biz_it.cabinet.db;

import org.apache.log4j.Logger;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import ru.biz_it.cabinet.web.shared.CertificateData;
import ru.biz_it.cabinet.web.shared.DataBean;
import ru.biz_it.cabinet.web.shared.IDataObject;
import ru.biz_it.cabinet.web.shared.ListBean;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

@Repository
public class CertificateDAO implements CabinetDAO {

    Logger log = Logger.getLogger(CertificateDAO.class);

    private static final String INSERT_STATEMENT = "INSERT INTO certs (mac, email, issue_date, end_date, reg_key, user_id, data, principal_name, org_name, version, inactive) VALUES (:macAddress, :email, :issueDate, :endDate, :regKey, :userId, :data, :principalName, :orgName, :version, 0)";
    private static final String UPDATE_STATEMENT = "UPDATE certs SET mac=:macAddress, email=:email, issue_date=:issueDate, end_date=:endDate, reg_key=:regKey, user_id=:userId, inactive=:inactive, principal_name=:principalName, org_name=:orgName, version=:version WHERE _id=:id";
    private static final String SELECT_STATEMENT = "SELECT certs._id, certs.mac, certs.email, certs.issue_date, certs.end_date, certs.reg_key, certs.user_id, certs.data, certs.version, certs.inactive, certs.principal_name, certs.org_name FROM certs ";
    private static final String SELECT_STATEMENT_ORG = "SELECT certs._id, certs.mac, certs.email, certs.issue_date, certs.end_date, certs.reg_key, certs.user_id, certs.data, certs.version, certs.inactive, certs.principal_name, certs.org_name FROM certs INNER JOIN users ON certs.user_id = users._id";

    private static final String DELETE_STATEMENT = "DELETE FROM certs WHERE _id=:id";

    private NamedParameterJdbcTemplate jdbcTemplate;


    private class CertRowMapper implements RowMapper<CertificateData> {

        Map<String, String> columns = new Hashtable<String, String>();

        private CertRowMapper() {
            columns.put("mac", "certs.mac");
            columns.put("email", "certs.email");
            columns.put("issueDate", "certs.issue_date");
            columns.put("endDate", "certs.end_date");
            columns.put("regKey", "certs.reg_key");
            columns.put("userId", "certs.user_id");
            columns.put("orgName", "certs.org_name");
            columns.put("data", "certs.data");
            columns.put("inactive", "certs.inactive");
            columns.put("id", "certs._id");
            columns.put("principalName", "certs.principal_name");
            columns.put("version", "certs.version");
            columns.put("orgId", "users.org_id");
        }

        @Override
        public CertificateData mapRow(ResultSet resultSet, int i) throws SQLException {
            CertificateData certificateData = new CertificateData(resultSet.getString("email"), resultSet.getString("mac"));
            certificateData.setId(resultSet.getInt("_id"));
            certificateData.setIssueDate(resultSet.getDate("issue_date"));
            certificateData.setEndDate(resultSet.getDate("end_date"));
            certificateData.setRegKey(resultSet.getString("reg_key"));
            UserDAO userDAO = new UserDAO();
            userDAO.setJdbcTemplate(jdbcTemplate);
            certificateData.setUserData(userDAO.read(resultSet.getInt("user_id"), null));
            certificateData.setUserId(resultSet.getInt("user_id"));
            certificateData.setData(resultSet.getBytes("data"));
            certificateData.setInactive(resultSet.getBoolean("inactive"));
            certificateData.setPrincipalName(resultSet.getString("principal_name"));
            certificateData.setOrgName(resultSet.getString("org_name"));
            certificateData.setVersion(resultSet.getString("version"));
            return certificateData;
        }
    }

    @Autowired
    public void setDataSource(DataSource dataSource) {
        jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public void save(final DataBean dataObject) {
        CabinetDAOService.SaveToDB(dataObject, INSERT_STATEMENT, UPDATE_STATEMENT, jdbcTemplate, new BeanPropertySqlParameterSource(dataObject));
//        CertificateData certificateData = (CertificateData)bean;
//        SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(certificateData);
//        if (certificateData.getId() == 0) {
//            KeyHolder keyHolder = new GeneratedKeyHolder();
//            jdbcTemplate.update(INSERT_STATEMENT, namedParameters, keyHolder);
//            certificateData.setId(keyHolder.getKey().intValue());
//        } else {
//            jdbcTemplate.update(UPDATE_STATEMENT, namedParameters);
//        }
    }

    @Override
    public int getCount(@Nullable String principalName) {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public CertificateData read(int id, @Nullable String principalName) {
        String query = SELECT_STATEMENT + " WHERE _id=:id";
        if (principalName != null) {
            query += " AND principal_name=:principalName";
        }
        Map<String, Object> params = new Hashtable<>();
        params.put("id", id);
        if (principalName != null)
            params.put("principalName", principalName);
        try {
            return jdbcTemplate.queryForObject(query, new MapSqlParameterSource(params), new CertRowMapper());
        } catch (EmptyResultDataAccessException ex) {
            log.error(ex.getMessage(), ex);
            return null;
        }
    }

    public CertificateData read(String mac, @Nullable String principalName) {
        String query = SELECT_STATEMENT + " WHERE mac=:macAddress";
        if (principalName != null)
            query += " AND principal_name=:principalName";
        query += " AND inactive=0 ORDER BY issue_date DESC";
        Map<String, Object> values = new HashMap<String, Object>();
        values.put("macAddress", mac);
        values.put("principalName", principalName);
        List<CertificateData> data = jdbcTemplate.query(query, new MapSqlParameterSource(values), new CertRowMapper());
        if (data.size() > 0)
            return data.get(0);
        return null;
    }

    public CertificateData read(String email, String mac, @Nullable String principalName) {
        String whereClause = "inactive=0";
        if (mac != null) {
            whereClause += " AND mac=:macAddress";
        }
        if (email != null) {
            whereClause += " AND email=:email";
        }
        if (principalName != null) {
            whereClause += " AND principal_name=:principalName";
        }
        Map<String, Object> values = new HashMap<String, Object>();
        values.put("macAddress", mac);
        values.put("principalName", principalName);
        values.put("email", email);

        String query = SELECT_STATEMENT + " WHERE " + whereClause + " ORDER BY issue_date DESC";
        List<CertificateData> data = jdbcTemplate.query(query, new MapSqlParameterSource(values), new CertRowMapper());
        if (data.size() == 0)
            return null;
        return data.get(0);
    }

    public List readList(ListBean listBean) {
        CertRowMapper mapper = new CertRowMapper();
        String query = SELECT_STATEMENT;
        if (listBean.getSelection().containsKey("orgId")) {
            query = SELECT_STATEMENT_ORG;
        }
        return jdbcTemplate.query(SimpleQueryBuilder.buildSelectQuery(query, listBean, mapper.columns, true),
                new MapSqlParameterSource(listBean == null ? null :listBean.getSelection()), mapper);
    }

    public int getSize(ListBean listBean) {
        CertRowMapper mapper = new CertRowMapper();
        String query = "SELECT COUNT(*) FROM certs";
        if (listBean.isActiveOnly()) {
            listBean.addSelection("inactive", 0);
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.YEAR, 1);
            listBean.addSelection("endDate", new Date(calendar.getTimeInMillis()));
            listBean.addSelectionOperation("endDate", ">");
        }

        return jdbcTemplate.queryForObject(SimpleQueryBuilder.buildSelectQuery(query, listBean, mapper.columns, false),
                new MapSqlParameterSource(listBean == null ? null : listBean.getSelection()), Integer.class).intValue();
    }

    @Override
    public void delete(DataBean dataBean) {
        SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(dataBean);
        jdbcTemplate.update(DELETE_STATEMENT, namedParameters);
    }
}
