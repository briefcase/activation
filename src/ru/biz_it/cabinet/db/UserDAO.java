package ru.biz_it.cabinet.db;

import org.apache.log4j.Logger;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import ru.biz_it.cabinet.web.shared.*;

import javax.sql.DataSource;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

@Repository
public class UserDAO implements CabinetDAO {

    Logger log = Logger.getLogger(CabinetDAO.class);

    private static final String INSERT_STATEMENT = "INSERT INTO users (mac, name, email, org_name, org_title, region, country, city, principal_name, inn, org_id) VALUES (:macAddress, :name, :email, :orgName, :orgTitle, :region, :country, :city, :principalName, :inn, :orgId)";
    private static final String UPDATE_STATEMENT = "UPDATE users SET mac=:macAddress, name=:name, email=:email, org_name=:orgName, org_title=:orgTitle, region=:region, country=:country, city=:city, principal_name=:principalName, inn=:inn, org_id=:orgId WHERE _id=:id";
    //private static final String SELECT_STATEMENT = "SELECT mac, name, email, org_name, org_title, region, country, city, _id, principal_name, inn, org_id FROM users";
    private static final String SELECT_STATEMENT = "SELECT users.mac, users.name, users.email, users.org_name, users.org_title, users.region, users.country, users.city, users._id, users.principal_name, users.inn, users.org_id FROM users";
    private static final String SELECT_STATEMENT_ACTIVE = "SELECT users.mac, users.name, users.email, users.org_name, users.org_title, users.region, users.country, users.city, users._id, users.principal_name, users.inn, users.org_id FROM users " +
            "INNER JOIN (SELECT DISTINCT user_id FROM certs WHERE NOT certs.inactive AND certs.end_date > :date) AS cert_data ON users._id=cert_data.user_id";
    private static final String SELECT_STATEMENT_KEY = "SELECT DISTINCT users.mac, users.name, users.email, users.org_name, users.org_title, users.region, users.country, users.city, users._id, users.principal_name, users.inn, users.org_id FROM users " +
            "INNER JOIN certs ON users._id=certs.user_id";
    private static final String DELETE_STATEMENT = "DELETE FROM users WHERE _id=:id";

    private NamedParameterJdbcTemplate jdbcTemplate;

    private class UserRowMapper implements RowMapper<UserData> {

        Map<String, String> columns = new Hashtable<>();

        public UserRowMapper() {
            columns.put("macAddress", "users.mac");
            columns.put("name", "users.name");
            columns.put("email", "users.email");
            columns.put("orgName", "users.org_name");
            columns.put("orgTitle", "users.org_title");
            columns.put("region", "users.region");
            columns.put("country", "users.country");
            columns.put("city", "users.city");
            columns.put("id", "users._id");
            columns.put("principalName", "users.principal_name");
            columns.put("inn", "users.inn");
            columns.put("orgId", "users.org_id");
            columns.put("key", "certs.reg_key");
        }

        @Override
        public UserData mapRow(ResultSet resultSet, int i) throws SQLException {
            UserData userData = new UserData(0);
            userData.setMacAddress(resultSet.getString("mac"));
            userData.setName(resultSet.getString("name"));
            userData.setEmail(resultSet.getString("email"));
            userData.setOrgName(resultSet.getString("org_name"));
            userData.setOrgTitle(resultSet.getString("org_title"));
            userData.setRegion(resultSet.getString("region"));
            userData.setCountry(resultSet.getString("country"));
            userData.setCity(resultSet.getString("city"));
            userData.setId(resultSet.getInt("_id"));
            userData.setPrincipalName(resultSet.getString("principal_name"));
            userData.setInn(resultSet.getString("inn"));
            userData.setOrgId(resultSet.getInt("org_id"));
            if (userData.getOrgId() != 0) {
                OrganizationDAO orgDAO = new OrganizationDAO();
                orgDAO.setJdbcTemplate(jdbcTemplate);
                userData.setOrganization(orgDAO.read(userData.getOrgId(), null));
            }
            return userData;
        }
    }

    @Autowired
    public void setDataSource(DataSource dataSource) {
        jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public void setJdbcTemplate(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }


    public void save(final DataBean dataObject) {
        CabinetDAOService.SaveToDB(dataObject, INSERT_STATEMENT, UPDATE_STATEMENT, jdbcTemplate, new BeanPropertySqlParameterSource(dataObject));
//        UserData userData = (UserData) bean;
//        SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(userData);
//        if (userData.getId() == 0) {
//            KeyHolder keyHolder = new GeneratedKeyHolder();
//            jdbcTemplate.update(INSERT_STATEMENT, namedParameters, keyHolder);
//            userData.setId(keyHolder.getKey().intValue());
//        } else {
//            jdbcTemplate.update(UPDATE_STATEMENT, namedParameters);
//        }
    }

    @Override
    public int getCount(@Nullable String principalName) {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public UserData read(int id, @Nullable String principalName) {
        try {
            String query = SELECT_STATEMENT + " WHERE _id=:id";
            if (principalName != null) {
                query += " AND principal_name=:principalName";
            }
            Map<String, Object> params = new HashMap<>();
            params.put("id", id);
            params.put("principalName", principalName);
            return jdbcTemplate.queryForObject(query,
                    new MapSqlParameterSource(params), new UserRowMapper());
        } catch (EmptyResultDataAccessException ex) {
            log.error(ex.getMessage(), ex);
            return null;
        }
    }

    public UserData read(String mac) {
        try {
            return jdbcTemplate.queryForObject(SELECT_STATEMENT + " WHERE mac=:macAddress",
                    new MapSqlParameterSource("macAddress", mac), new UserRowMapper());
        } catch (EmptyResultDataAccessException ex) {
            log.error(ex.getMessage(), ex);
            return null;
        }
    }


    public List readList(ListBean listBean) {
        UserRowMapper mapper = new UserRowMapper();
        String query = SELECT_STATEMENT;
        if (listBean != null && listBean.getSelection().containsKey("key")) {
            query = SELECT_STATEMENT_KEY;
        } else if (listBean != null && ((UserListBean)listBean).isActiveOnly()) {
            query = SELECT_STATEMENT_ACTIVE;
        }
        HashMap<String, Object> parameters = null;
        if (listBean != null) {
            parameters = listBean.getSelection();
            parameters.put("date", new Date(System.currentTimeMillis()));
        }
        return jdbcTemplate.query(SimpleQueryBuilder.buildSelectQuery(query, listBean, mapper.columns, true),
                new MapSqlParameterSource(parameters), mapper);
    }

    public int getSize(ListBean listBean) {
        UserRowMapper mapper = new UserRowMapper();
        String query = listBean != null && listBean.isActiveOnly() ?
                "SELECT COUNT(*) FROM users INNER JOIN (SELECT DISTINCT user_id FROM certs WHERE NOT certs.inactive AND certs.end_date > :date) AS cert_data ON users._id=cert_data.user_id" :
                "SELECT COUNT(*) FROM users";
        HashMap<String, Object> parameters = null;
        if (listBean != null) {
            parameters = listBean.getSelection();
            parameters.put("date", new Date(System.currentTimeMillis()));
        }
        return jdbcTemplate.queryForObject(SimpleQueryBuilder.buildSelectQuery(query, listBean, mapper.columns, false),
                new MapSqlParameterSource(parameters), Integer.class);
    }

    @Override
    public void delete(DataBean dataBean) {
        SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(dataBean);
        jdbcTemplate.update(DELETE_STATEMENT, namedParameters);
    }
}

