package ru.biz_it.cabinet.db;

import org.apache.log4j.Logger;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import ru.biz_it.cabinet.web.shared.DataBean;
import ru.biz_it.cabinet.web.shared.IDataObject;
import ru.biz_it.cabinet.web.shared.ListBean;
import ru.biz_it.cabinet.web.shared.Organization;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

@Repository
public class OrganizationDAO implements CabinetDAO {

    Logger log = Logger.getLogger(CabinetDAO.class);

    private static final String INSERT_STATEMENT = "INSERT INTO orgs (org_name, region, country, city, principal_name, inn) VALUES (:orgName, :region, :country, :city, :principalName, :inn)";
    private static final String UPDATE_STATEMENT = "UPDATE orgs SET org_name=:orgName, region=:region, country=:country, city=:city, principal_name=:principalName, inn=:inn WHERE _id=:id";
    private static final String SELECT_STATEMENT = "SELECT org_name, region, country, city, _id, principal_name, inn FROM orgs";
    private static final String DELETE_STATEMENT = "DELETE FROM orgs WHERE _id=:id";

    private NamedParameterJdbcTemplate jdbcTemplate;

    private class OrganizationRowMapper implements RowMapper<Organization> {

        Map<String, String> columns = new Hashtable<>();

        public OrganizationRowMapper() {
            columns.put("orgName", "org_name");
            columns.put("region", "region");
            columns.put("country", "country");
            columns.put("city", "city");
            columns.put("id", "_id");
            columns.put("principalName", "principal_name");
            columns.put("inn", "inn");
        }

        @Override
        public Organization mapRow(ResultSet resultSet, int i) throws SQLException {
            Organization organization = new Organization(resultSet.getInt("_id"));
            organization.setOrgName(resultSet.getString("org_name"));
            organization.setRegion(resultSet.getString("region"));
            organization.setCountry(resultSet.getString("country"));
            organization.setCity(resultSet.getString("city"));
            organization.setPrincipalName(resultSet.getString("principal_name"));
            organization.setInn(resultSet.getString("inn"));
            return organization;
        }
    }

    @Autowired
    public void setDataSource(DataSource dataSource) {
        jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public void setJdbcTemplate(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void save(final DataBean dataObject) {
        CabinetDAOService.SaveToDB(dataObject, INSERT_STATEMENT, UPDATE_STATEMENT, jdbcTemplate, new BeanPropertySqlParameterSource(dataObject));
//        Organization organization = (Organization) bean;
//        SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(organization);
//        if (organization.getId() == 0) {
//            KeyHolder keyHolder = new GeneratedKeyHolder();
//            jdbcTemplate.update(INSERT_STATEMENT, namedParameters, keyHolder);
//            organization.setId(keyHolder.getKey().intValue());
//        } else {
//            jdbcTemplate.update(UPDATE_STATEMENT, namedParameters);
//        }
    }

    @Override
    public int getCount(@Nullable String principalName) {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Organization read(int id, @Nullable String principalName) {
        try {
            String query = SELECT_STATEMENT + " WHERE _id=:id";
            if (principalName != null) {
                query += " AND principal_name=:principalName";
            }
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("id", id);
            params.put("principalName", principalName);
            return jdbcTemplate.queryForObject(query,
                    new MapSqlParameterSource(params), new OrganizationRowMapper());
        } catch (EmptyResultDataAccessException ex) {
            log.error(ex.getMessage(), ex);
            return null;
        }
    }

    public Organization read(String mac) {
        try {
            return jdbcTemplate.queryForObject(SELECT_STATEMENT + " WHERE mac=:macAddress",
                    new MapSqlParameterSource("macAddress", mac), new OrganizationRowMapper());
        } catch (EmptyResultDataAccessException ex) {
            log.error(ex.getMessage(), ex);
            return null;
        }
    }


    public List readList(ListBean listBean) {
        OrganizationRowMapper mapper = new OrganizationRowMapper();
        return jdbcTemplate.query(SimpleQueryBuilder.buildSelectQuery(SELECT_STATEMENT, listBean, mapper.columns, true),
                new MapSqlParameterSource(listBean == null ? null : listBean.getSelection()), mapper);
    }

    public int getSize(ListBean listBean) {
        OrganizationRowMapper mapper = new OrganizationRowMapper();
        return jdbcTemplate.queryForObject(SimpleQueryBuilder.buildSelectQuery("SELECT COUNT(*) FROM orgs", listBean, mapper.columns, false),
                new MapSqlParameterSource(listBean == null ? null :listBean.getSelection()), Integer.class);
    }

    @Override
    public void delete(DataBean dataBean) {
        SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(dataBean);
        jdbcTemplate.update(DELETE_STATEMENT, namedParameters);
    }
}

