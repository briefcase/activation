package ru.biz_it.cabinet.db;

import ru.biz_it.cabinet.web.shared.ListBean;

import java.util.Map;

public class SimpleQueryBuilder {

    public static String buildSelectQuery(String selectQuery, ListBean listBean, Map<String, String> columns, boolean useLimits) {
        String whereClause = "";
        String orderClause = "";
        String limitClause = "";
        if (listBean != null) {
            for (String paramName : listBean.getSelection().keySet()) {
                if (listBean.getSelection().get(paramName) != null && columns.get(paramName) != null) {
                    whereClause += "".equals(whereClause) ? " WHERE " : " AND ";
                    if ("principalName".equals(paramName)) {
                        whereClause += "(" + columns.get(paramName) + "=:" + paramName + " OR " + columns.get(paramName) + " IS NULL)";
                    } else {
                        String operation = listBean.getSelectionOperations().get(paramName);
                        if (operation == null)
                            operation = "=";
                        if("userList".equals(listBean.getListActionName()) && "name".equals(paramName)) {
                            whereClause += "(" + columns.get(paramName) + operation + ":" + paramName +
                                    " OR " + columns.get("macAddress") + operation + ":" + paramName +
                                    " OR " + columns.get("email") + operation + ":" + paramName + ")";
                        } else {
                            whereClause += columns.get(paramName) + operation + ":" + paramName;
                        }
                    }
                }
            }
            for (String order : listBean.getOrder()) {
                if (columns.get(order) != null) {
                    orderClause += ("".equals(orderClause) ? "" : ",") + columns.get(order);
                    if (listBean.isDesc())
                        orderClause += " DESC";
                }
            }
            if (useLimits && listBean.getCount() != 0) {
                limitClause += " LIMIT " + listBean.getStart() + "," + listBean.getCount();
            }
        }
        return selectQuery + whereClause + ("".equals(orderClause) ? "" : " ORDER BY ") + orderClause + limitClause;
    }
}
