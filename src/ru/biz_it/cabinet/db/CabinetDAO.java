package ru.biz_it.cabinet.db;

import org.springframework.beans.factory.annotation.Autowired;
import ru.biz_it.cabinet.web.shared.DataBean;
import ru.biz_it.cabinet.web.shared.IDataObject;
import ru.biz_it.cabinet.web.shared.ListBean;

import javax.sql.DataSource;
import java.util.List;

public interface CabinetDAO {

    public List readList(ListBean listBean);
    public int getSize(ListBean listBean);

    @Autowired
    public void setDataSource(DataSource dataSource);

    public void delete(DataBean dataBean);
    
    public DataBean read(int id, String principalName);

    public void save(final DataBean dataObject);

    public int getCount(String principalName);

}
