package ru.biz_it.cabinet;

import org.bouncycastle.crypto.CryptoException;
import org.bouncycastle.openssl.PEMWriter;
import org.eu.mayrhofer.channel.X509CertificateGenerator;
import ru.biz_it.cabinet.db.RegistrationKeyDAO;
import ru.biz_it.cabinet.web.shared.CertificateData;
import ru.biz_it.cabinet.web.shared.RegistrationKey;
import ru.biz_it.cabinet.web.shared.UserData;

import java.io.*;
import java.security.*;
import java.security.cert.CRLException;
import java.security.cert.CertificateException;
import java.security.spec.InvalidKeySpecException;
import java.util.Calendar;

public class CertificateUtils  {

    //public static ApplicationContext CONTEXT = new ClassPathXmlApplicationContext("springapp-servlet.xml");
//    public static final WebApplicationContext CONTEXT = WebApplicationContextUtils.getRequiredWebApplicationContext(MobileClientServlet);

    private X509CertificateGenerator certificateGenerator;

    private String caFile;
    private String caPassword;
    private String caAlias;
    private String crlFile;
    private boolean useBCAPI;
    private String password;
    private String alias;
    private String version;

    private String storagePath;

    public String getCaFile() {
        return caFile;
    }

    public void setCaFile(String caFile) {
        this.caFile = caFile;
    }

    public String getCaPassword() {
        return caPassword;
    }

    public void setCaPassword(String caPassword) {
        this.caPassword = caPassword;
    }

    public String getCaAlias() {
        return caAlias;
    }

    public void setCaAlias(String caAlias) {
        this.caAlias = caAlias;
    }

    public String getCrlFile() {
        return crlFile;
    }

    public void setCrlFile(String crlFile) {
        this.crlFile = crlFile;
    }

    public boolean isUseBCAPI() {
        return useBCAPI;
    }

    public void setUseBCAPI(boolean useBCAPI) {
        this.useBCAPI = useBCAPI;
    }

    public String getStoragePath() {
        return storagePath;
    }

    public void setStoragePath(String storagePath) {
        this.storagePath = storagePath;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public void writeCRL(Writer writer) throws Exception {

        X509CertificateGenerator certificateGenerator = getCertificateGenerator();
        PEMWriter pemWriter = new PEMWriter(writer);
        pemWriter.writeObject(certificateGenerator.getCrl());
        pemWriter.close();
    }
    
    public void writeSpecialCertificate(SpecialCertificateData specialCertificateData, Writer writer) throws IOException {
        InputStream is = new FileInputStream(storagePath + specialCertificateData.getFileName());
        byte[] buffer = new byte[2048];
        for (int l; (l = is.read(buffer)) > -1 ; ) {
            writer.write(new String(buffer, 0, l));
        }
    }

    public X509CertificateGenerator getCertificateGenerator() throws SignatureException, CRLException, IOException, NoSuchProviderException, InvalidKeyException, NoSuchAlgorithmException, KeyStoreException, CertificateException, UnrecoverableKeyException {
        if (certificateGenerator == null) {
            synchronized (X509CertificateGenerator.class) {
                certificateGenerator = new X509CertificateGenerator(storagePath + caFile, caPassword, caAlias, storagePath + crlFile, useBCAPI);
            }
        }
        return certificateGenerator;
    }

    public void setCertificateGenerator(X509CertificateGenerator certificateGenerator) {
        this.certificateGenerator = certificateGenerator;
    }

    public X509CertificateGenerator.CertData getCertData(CertificateData certificateData) throws IOException, NoSuchAlgorithmException, KeyStoreException, CertificateException, UnrecoverableKeyException, SignatureException, CRLException, InvalidKeyException, NoSuchProviderException {
        InputStream is = new ByteArrayInputStream(certificateData.getData());
        X509CertificateGenerator.CertData certData = getCertificateGenerator().restoreCertificate(is, getPassword(), getAlias());
        is.close();
        return certData;
    }

    public synchronized void revokeCertificate(CertificateData certificateData) throws IOException, GeneralSecurityException {
        X509CertificateGenerator certificateGenerator = getCertificateGenerator();
        certificateData.setInactive(true);
        certificateGenerator.generateCRL(getCertData(certificateData).cert);
        certificateGenerator.saveCRL(storagePath + crlFile);
    }

    public CertificateData createCertificate(UserData userData, RegistrationKey key, int validityDays, CertificateData oldCert)
            throws SignatureException, CryptoException, IOException, NoSuchProviderException, InvalidKeyException, InvalidKeySpecException, NoSuchAlgorithmException, KeyStoreException, CertificateException, CRLException, UnrecoverableKeyException {

        String newCertVersion = version;
        if (oldCert != null)
            newCertVersion = oldCert.getVersion();
        if (key != null && !(key.getVersion() == null || "".equals(key.getVersion())))
            newCertVersion = key.getVersion();
        X509CertificateGenerator certificateGenerator = getCertificateGenerator();
        X509CertificateGenerator.CertData certData = certificateGenerator.createCertificate(
                userData.getName(), userData.getEmail(), userData.getMacAddress(), userData.getOrgTitle(),
                userData.getOrgName(), userData.getRegion(), userData.getCountry(), userData.getCity() , newCertVersion, validityDays);
        CertificateData certificateData = new CertificateData(userData.getEmail(), userData.getMacAddress());
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        certificateGenerator.saveCertificate(certData, out, password, alias);
        out.close();
        certificateData.setData(out.toByteArray());
        certificateData.setIssueDate(certData.cert.getNotBefore());
        Calendar expiry = Calendar.getInstance();
        expiry.add(Calendar.DAY_OF_YEAR, validityDays);
        certificateData.setEndDate(expiry.getTime());
        if (key != null) {
            certificateData.setRegKey(key.getKey());
        }
        certificateData.setUserData(userData);
        certificateData.setVersion(newCertVersion);
        if (key != null) {
            certificateData.setOrgName(key.getOrgName());
            certificateData.setPrincipalName(key.getPrincipalName());
        } else if (oldCert != null){
            certificateData.setOrgName(oldCert.getOrgName());
            certificateData.setPrincipalName(oldCert.getPrincipalName());
        } else {
            certificateData.setOrgName(userData.getOrgName());
            certificateData.setPrincipalName(userData.getPrincipalName());
        }
        return certificateData;
    }

    public void saveKey(RegistrationKey key, RegistrationKeyDAO keyDAO) {
        keyDAO.save(key, true);
    }

    public int getNewCertificateValidity(CertificateData certificateData) {
        long millis = certificateData.getEndDate().getTime() + 24 * 60 * 60 * 1000 - System.currentTimeMillis();
        return (int) (millis / (1000 * 60 * 60 * 24));
    }



}
