package ru.biz_it.cabinet;

import org.apache.log4j.Logger;
import org.bouncycastle.crypto.CryptoException;
import org.bouncycastle.openssl.PEMWriter;
import org.jetbrains.annotations.Nullable;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.DispatcherServlet;
import ru.biz_it.cabinet.db.CertificateDAO;
import ru.biz_it.cabinet.db.RegistrationKeyDAO;
import ru.biz_it.cabinet.db.UserDAO;
import ru.biz_it.cabinet.web.shared.CertificateData;
import ru.biz_it.cabinet.web.shared.RegistrationKey;
import ru.biz_it.cabinet.web.shared.UserData;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Writer;
import java.security.*;
import java.security.cert.CRLException;
import java.security.cert.CertificateException;
import java.util.Map;
import java.util.regex.Pattern;

public class MobileClientServlet extends DispatcherServlet {

    Logger log = Logger.getLogger(DispatcherServlet.class);

    private CertificateUtils utils;
    private RegistrationKeyDAO keyDAO;
    private UserDAO userDAO;
    private CertificateDAO certificateDAO;
    private Map<String, SpecialCertificateData> specialCertificates;

    private static final Pattern rfc2822 = Pattern.compile(
            "^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$"
    );

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        this.utils = WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext()).getBean(CertificateUtils.class);
        this.keyDAO = WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext()).getBean(RegistrationKeyDAO.class);
        this.userDAO = WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext()).getBean(UserDAO.class);
        this.certificateDAO = WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext()).getBean(CertificateDAO.class);
        this.specialCertificates = WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext()).getBeansOfType(SpecialCertificateData.class);
    }

    @Override
    protected void doDispatch(HttpServletRequest request, HttpServletResponse response) throws Exception {
        doAction(request, response);
    }

    private void doAction(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.debug(req.getRequestURI() + "?" + req.getQueryString());
        if (req.getRequestURI().endsWith(".crl")) {
            try {
                utils.writeCRL(resp.getWriter());
            } catch (Exception ex){
                log.error(ex.getMessage(), ex);
                throw new ServletException(ex);
            }
        } else if(req.getRequestURI().endsWith(".crt")) {
            String mac = req.getParameter("id");
            String email = req.getParameter("mail");
            if (email != null && !verifyEmail(email)) {
                resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "Wrong e-mail format");
                return;
            }
            String regKey = req.getParameter("reg");
            log.info("regKey: " + regKey);
            if (mac == null) {
                resp.sendError(HttpServletResponse.SC_EXPECTATION_FAILED, "Expected parameter 'id' is missing");
                return;
            }
            processCertificate(resp, mac, email, regKey);
        } else if (req.getRequestURI().endsWith(".reg")) {
            registerUser(req, resp);
        }
    }

    private void registerUser(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String email = request.getParameter("mail");
        if (!verifyEmail(email)) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Wrong e-mail format");
            return;
        }
        String mac = request.getParameter("id");

        String name = request.getParameter("name");
        String inn = request.getParameter("inn");
        String orgName = request.getParameter("orgName");
        String orgTitle = request.getParameter("orgTitle");
        String country = request.getParameter("country");
        String city = request.getParameter("city");
        String region = request.getParameter("region");
        if (mac == null || "".equals(mac)) {
            response.sendError(HttpServletResponse.SC_EXPECTATION_FAILED, "Expected parameter 'id' is missing");
            return;
        }
        if (email == null || "".equals(email)) {
            response.sendError(HttpServletResponse.SC_EXPECTATION_FAILED, "Expected parameter 'mail' is missing");
            return;
        }
        String oldMac = null;
        if (mac.indexOf(',') > 0) {
            String[] macs = mac.split(",");
            mac = macs[0];
            oldMac = macs[1];
        }
        UserData userData = null;
        if (oldMac != null) {
            userData = userDAO.read(oldMac);
            if (userData != null) {
                userData.setMacAddress(mac);
            }
        }
        if (userData == null)
            userData = userDAO.read(mac);
        RegistrationKey key = null;
        if (userData == null) {
            userData = new UserData(mac);
            try {
                key = new RegistrationKey();
                key.setKey(KeyGenerator.generateKey());
                key.setOrgName(orgName);
                key.setValidDays(10);
                key.setMacAddress(mac);
                keyDAO.save(key, true);
            } catch (Exception ex){
                log.error(ex.getMessage(), ex);
                throw new ServletException(ex);
            }
        } else if (!email.equals(userData.getEmail())) {
            CertificateData certificateData = oldMac == null ? certificateDAO.read(mac, null) : certificateDAO.read(oldMac, null);
            if (certificateData != null) {
                try {
                    reIssueCertificate(response, email, certificateData, utils.getNewCertificateValidity(certificateData), userData, null);
                } catch (Exception ex) {
                    log.error(ex.getMessage(), ex);
                    throw new ServletException(ex);
                }
            }
        }
        userData.setEmail(email);
        if (orgName != null)
            userData.setOrgName(orgName);
        if (name != null)
            userData.setName(name);
        if (city != null)
            userData.setCity(city);
        if (country != null)
            userData.setCountry(country);
        if (region != null)
            userData.setRegion(region);
        if (orgTitle != null)
            userData.setOrgTitle(orgTitle);
        if (inn != null)
            userData.setInn(inn);
        userDAO.save(userData);
        if (key == null) {
            response.sendError(HttpServletResponse.SC_FOUND);
        } else {
            Writer writer = response.getWriter();
            writer.write(key.getKey());
            writer.close();
        }

    }

    private void processCertificate(HttpServletResponse resp, String mac, String email, String regKey) throws IOException, ServletException {
        boolean blankEmail = email == null || "".equals(email);
        boolean blankKey = regKey == null || "".equals(regKey);
        String certId = mac;
        String oldCertId = null;
        if (certId.indexOf(',') > 0) {
            String[] ids = certId.split(",");
            certId = ids[0];
            oldCertId = ids[1];
        }
        CertificateData certificateData = null;
        if (oldCertId != null) {
            certificateData = certificateDAO.read(oldCertId, null);
            if (certificateData == null) {
//                String oldId = oldCertId;
//                oldCertId = certId;
//                certId = oldId;
                certificateData = certificateDAO.read(certId, null);
            }
            if (certificateData != null)
                certificateData.setMacAddress(certId);
        }
        boolean specialEmail = checkIfEmailSpecial(resp, email);

        if (blankEmail && (!blankKey || certificateData == null)) {
            log.debug("Expected parameter 'mail' is missing");
            resp.sendError(HttpServletResponse.SC_EXPECTATION_FAILED, "Expected parameter 'mail' is missing");
            return;
        }
        if (specialEmail) {
            log.debug("Email is special");
            return;
        }

        if (blankKey) {
            if (certificateData != null) {
                log.debug("Updating certificate");
                updateAndSendCertificate(resp, email, certificateData);
            } else {
                UserData userData = oldCertId == null ? userDAO.read(certId) : userDAO.read(oldCertId);
                if (userData == null)
                    userData = new UserData(certId);
                else if (oldCertId != null)
                    userData.setMacAddress(certId);

                try {
                    log.debug("Re-issuing certificate");
                    reIssueCertificate(resp, email, certificateData, 10, userData, null);
                } catch (Exception ex) {
                    log.error(ex.getMessage(), ex);
                    throw new ServletException(ex);
                }
            }
        } else {
            RegistrationKey key = keyDAO.read(regKey);
            if (key == null) {
                log.debug("Key not found");
                resp.sendError(HttpServletResponse.SC_FORBIDDEN, "Key not found");
            } else if (!key.isApproved()) {
                log.debug("Key not approved");
                resp.sendError(HttpServletResponse.SC_FORBIDDEN, "Key not approved");
            } else if(key.getCertificateId() == 0) {
                try {
                    UserData userData = oldCertId == null ? userDAO.read(certId) : userDAO.read(oldCertId);
                    if (userData == null) {
                        userData = new UserData(certId);
                    } else if (oldCertId != null)
                        userData.setMacAddress(certId);
                    if (userData != null) {
                        userData.setEmail(email);
                        if (key.getOrgId() != 0) {
                            userData.setOrganization(key.getOrganization());
                            userData.copyOrganizationFields();
                        }
                        userDAO.save(userData);
                        log.debug("Re-issuing certificate");
                        reIssueCertificate(resp, email, certificateData, key.getValidDays(), userData, key);
                    }
                } catch (Exception ex) {
                    log.error(ex.getMessage(), ex);
                    throw new ServletException(ex);
                }
            } else {
                log.debug("Key already activated");
                resp.sendError(HttpServletResponse.SC_CONFLICT, "Key already activated");
            }
        }
    }

    private boolean checkIfEmailSpecial(HttpServletResponse resp, String email) throws IOException {
        SpecialCertificateData specialEmail = null;
        if (email != null && !"".equals(email)) {
            for (SpecialCertificateData specialData : specialCertificates.values()) {
                if (specialData.getEmail().equals(email)) {
                    specialEmail = specialData;
                    break;
                }
            }
        }
        if (specialEmail != null) {
            sendSpecialCertificate(resp, specialEmail);
        }
        return specialEmail != null;
    }

    private void updateAndSendCertificate(HttpServletResponse resp, String email, CertificateData certificateData) throws ServletException {
        log.debug("email: " + email);
        log.debug("cert email: " + certificateData.getEmail());
        log.debug("cert device ID: " + certificateData.getMacAddress());
        if (email == null || "".equals(email) || certificateData.getEmail().equals(email)) {
            try {
                writeDataToResponse(resp, certificateData);
            } catch (Exception ex) {
                log.error(ex.getMessage(), ex);
                throw new ServletException(ex);
            }
        } else {
            try {
                UserData userData = certificateData.getUserData();
                log.debug("Re-issuing certificate");
                reIssueCertificate(resp, email, certificateData, utils.getNewCertificateValidity(certificateData), userData, null);
            } catch (Exception ex) {
                log.error(ex.getMessage(), ex);
                throw new ServletException(ex);
            }
        }
    }

    private void reIssueCertificate(HttpServletResponse resp, String email, CertificateData certificateData,
                                    int validityDays, UserData userData, @Nullable RegistrationKey key)
            throws IOException, GeneralSecurityException, CryptoException {
        boolean updateUser= false;
        if (key != null && userData != null) {
            if (key.getOrgName() != null && !key.getOrgName().equals(userData.getOrgName())) {
                userData.setOrgName(key.getOrgName());
                updateUser = true;
            }
            if (key.getPrincipalName() != null && !key.getPrincipalName().equals(userData.getPrincipalName())) {
                userData.setPrincipalName(key.getPrincipalName());
                updateUser = true;
            }
        }
        if (userData != null) {
            if (!email.equals(userData.getEmail())) {
                userData.setEmail(email);
                updateUser = true;
            }
            if (certificateData != null) {
                if (userData.getPrincipalName() == null || "".equals(userData.getPrincipalName())) {
                    userData.setPrincipalName(certificateData.getPrincipalName());
                    updateUser = true;
                }
                if (userData.getOrgName() == null || "".equals(userData.getOrgName())) {
                    userData.setOrgName(certificateData.getOrgName());
                    updateUser = true;
                }
            }
        }
        if (updateUser)
            userDAO.save(userData);
        if (certificateData != null) {
            utils.revokeCertificate(certificateData);
            if (key != null) {
                certificateData.setOrgName(key.getOrgName());
                certificateData.setVersion(key.getVersion());
                if (certificateData.getPrincipalName() == null || "".equals(certificateData.getPrincipalName()))
                    certificateData.setPrincipalName(key.getPrincipalName());
                certificateData.setOrgName(key.getOrgName());
            }
            certificateDAO.save(certificateData);
        }
        CertificateData newCertificateData = utils.createCertificate(userData, key, validityDays, certificateData);
        certificateDAO.save(newCertificateData);
        if (key != null) {
            key.setCertificateId(newCertificateData.getId());
            keyDAO.save(key, false);
        }
        writeDataToResponse(resp, newCertificateData);

    }

    private void sendSpecialCertificate(HttpServletResponse resp, SpecialCertificateData specialEmail) throws IOException {
        utils.writeSpecialCertificate(specialEmail, resp.getWriter());
    }

    private void writeDataToResponse(HttpServletResponse resp, CertificateData certificateData) throws IOException, NoSuchAlgorithmException, KeyStoreException, CertificateException, UnrecoverableKeyException, SignatureException, CRLException, NoSuchProviderException, InvalidKeyException {
        PEMWriter pemWriter = new PEMWriter(resp.getWriter());
        pemWriter.writeObject(utils.getCertData(certificateData).cert);
        pemWriter.close();
    }

    private boolean verifyEmail(String email) {
        return rfc2822.matcher(email).matches();
    }

}
