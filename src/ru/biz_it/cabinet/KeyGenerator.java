package ru.biz_it.cabinet;

import java.security.NoSuchAlgorithmException;

public class KeyGenerator {

    public static String generateKey() throws java.security.NoSuchAlgorithmException {
//        String srcString = "";
//        for (int i = 0; i < 10; i++) {
//            char c = (char) Math.round(Math.random() * 256);
//            srcString += Character.toString(c);
//        }
//        String serialNumberEncoded = calculateSecurityHash(srcString,"MD2") +
//                                    calculateSecurityHash(srcString,"MD5") +
//                                    calculateSecurityHash(srcString,"SHA1") +
//                                    calculateSecurityHash(srcString,"SHA-256");
        String serialNumber = "";
        for (int i = 0; i < 4; i++) {
            for (int j = 0; (i % 2 == 1 && j < 5) || (i % 2 == 0 && j < 4); j ++ ) {
//                int key = (int)Math.round(Math.random() * 167);
//                serialNumber += serialNumberEncoded.charAt(key);
                serialNumber += Math.round(Math.random() * 9);
            }
            if (i != 3)
                serialNumber += "-";
        }
        return serialNumber;
    }

    private static String calculateSecurityHash(String stringInput, String algorithmName) throws java.security.NoSuchAlgorithmException {
        String hexMessageEncode = "";
        byte[] buffer = stringInput.getBytes();
        java.security.MessageDigest messageDigest = java.security.MessageDigest.getInstance(algorithmName);
        messageDigest.update(buffer);
        byte[] messageDigestBytes = messageDigest.digest();
        for (int countEncode : messageDigestBytes) {
            hexMessageEncode = hexMessageEncode + Integer.toString(countEncode & 0xff);
        }
        return hexMessageEncode;
    }

    public static void main(String[] args) throws Exception {
        System.out.println(generateKey());
    }
}
