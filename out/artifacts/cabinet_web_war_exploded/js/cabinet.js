///////////////////
// certList
//////////////////

var userDlg;

function editUser(userId) {
    var form = document.forms.editUserForm;
    form.elements.id.value = userId;
    dijit.byId("saveUserButton").set('disabled', false);
    dijit.byId("cancelUserButton").set('disabled', false);
    dojo.xhrGet({
        url:"#springUrl('/secure/viewUser.json')?id=" + userId,
        handleAs:"json",
        load: function(data){
            for(var i in data){
                var elem =dijit.byId("user" + i);
                if (elem != null ) {
                    if (data[i] != null)
                        elem.setValue(data[i]);
                    else
                        elem.setValue("");

                }

            }
            userDlg = dijit.byId("userDlg");
            userDlg.show();
        }
    });
}

function endsWith(str, suffix) {
    return str.indexOf(suffix, str.length - suffix.length) !== -1;
}

function saveUser() {
    var form = document.forms.editUserForm;
    for(var i = 0; i < form.elements.length; i++) {
        var field = form.elements[i];
        if (field.name != "id" && field.type == "hidden") {
            field.value = dijit.byId("user" + field.name).getValue();
        }
    }
    var json = dojo.formToJson("editUserForm");
    dijit.byId("saveUserButton").set('disabled', true);
    dijit.byId("cancelUserButton").set('disabled', true);
    dojo.xhrPut({
        url: "#springUrl('/secure/saveUser.json')",
        putData: json,
        handleAs: "json",
        headers: { "Content-Type": "application/json"},
        load: function(data, ioArgs){
            if (data.result == "OK") {
                userDlg.hide();
                dojo.forEach(dojo.query(".bluebutton"), function(button, i) {
                    for (var l = 0; l < button.attributes.length; l++) {
                        if (button.attributes[l].name == "widgetid" && endsWith(button.attributes[l].value, "_usr_" + form.id.value)) {
                            dijit.byId(button.attributes[l].value).set('label', form.name.value);
                        }
                    }
                });
            } else {

            }
        },
        error: function(err, ioArgs){
            console.error(err); // display the error
            //dojo.byId(componentName).style.backgroundColor = "#FF0000";
        }
    });

}

function updateCertificate(certId, fieldName, fieldValue) {
    var componentName = fieldName + "_" + certId;
    var form = dojo.byId("updateCertForm");
    for (var i = 0; i < form.elements.length; i++) {
        form.elements[i].value = form.elements[i].name == fieldName ? fieldValue : null;
    }
    dojo.byId("updateCertForm").id.value = certId;
    dojo.byId(componentName).style.backgroundColor = "#0000FF";

    dojo.xhrPost({
        url: "#springUrl('/secure/updateCert.json')",
        handleAs: "json",
        content: {
            certId: certId,
            fieldName: fieldName,
            fieldValue: fieldValue},
        load: function(data, ioArgs){
            if (data.result == "OK") {
                dojo.byId(componentName).style.backgroundColor = "#FFFFFF";
            } else {
                dojo.byId(componentName).style.backgroundColor = "#FF0000";
            }
        },
        error: function(err, ioArgs){
            console.error(err); // display the error
            //dojo.byId(componentName).style.backgroundColor = "#FF0000";
        }
    });
}

function revokeCertificate(certId, button) {
    button.set('disabled', true);
    dojo.xhrPost({
        url: "#springUrl('/secure/revokeCert.json')",
        content: {id: certId},
        load: function(data, ioArgs) {
            button.destroy(false);
        },
        error: function(err, ioArgs){
            console.error(err); // display the error
        }
    });
}

function reissueCertificate(certId, button) {
    button.set('disabled', true);
    dojo.xhrPost({
        url: "#springUrl('/secure/reissueCert.json')",
        content: {id: certId},
        load: function(data, ioArgs) {
            button.destroy(false);
        },
        error: function(err, ioArgs){
            console.error(err); // display the error
        }
    });
}

function deleteCertificate(certId, button) {
    button.set('disabled', true);
    dojo.xhrPost({
        url: "#springUrl('/secure/deleteCert.json')",
        content: {id: certId},
        load: function(data, ioArgs) {
            button.destroy(false);
        },
        error: function(err, ioArgs){
            console.error(err); // display the error
        }
    });
}

//////////////
// keyList
//////////////
var certDlg;
var generateDlg;
var listStore;
var orgList;

function generateKeysDialog() {
    var form = document.forms.generateKeysForm;
    dijit.byId("generateButton").set('disabled', false);
    dijit.byId("cancelGenerateButton").set('disabled', false);
    generateDlg = dijit.byId("generateKeysDlg");
    if (!orgList) {
        dojo.xhrGet({
            url:"#springUrl('/secure/orgList.json')?orderorgName=1",
            handleAs:"json",
            load: function(data){
                orgList = data;
                var select = dijit.byId("orgIdSelect");
                dojo.forEach(data, function(element, i) {
                    select.addOption({
                        label: element.orgName,
                        value: element.id
                    })
                });

                generateDlg.show();
            }
        });
    } else {
        generateDlg.show();
    }
}

function generateKeys() {
    var form = document.forms.generateKeysForm;
    dijit.byId("generateButton").set('disabled', true);
    dijit.byId("cancelGenerateButton").set('disabled', true);
    dojo.xhrPost({
        form: "generateKeysForm",
        handleAs: "json",
        load: function(data, ioArgs){
            generateDlg.hide();
            for (var i = 0; i < data.length; i++) {
                //listStore.newItem(data[i]);
                addRow(data[i]);
            }
        },
        error: function(err, ioArgs){
            console.error(err); // display the error
            //dojo.byId(componentName).style.backgroundColor = "#FF0000";
        }
    });
}

function addRow(keyData) {
    var table = document.getElementById('keysTable');
    var rowCount = table.rows.length;
    var row = table.insertRow(rowCount);

    var cell = row.insertCell(0);
    cell.innerHTML = keyData.key;
    cell = row.insertCell(1);
    cell.innerHTML = keyData.version;
    cell = row.insertCell(2);
    cell.innerHTML = keyData.validDays;
    cell = row.insertCell(3);
    cell.innerHTML = keyData.orgName;
    cell = row.insertCell(4);
    cell.innerHTML = keyData.principalName;
    row.insertCell(5);
    row.insertCell(6);
    cell = row.insertCell(7);
    cell.innerHTML = '<button id="approve_' + keyData.key + '" type="button"></button>';
    new dijit.form.Button({
        label: "#springMessage('approve.caption')",
        onClick: function() {
            approveKey(keyData.key);
        }
    }, "approve_" + keyData.key);
    cell = row.insertCell(8);
    cell.innerHTML = '<button id="delete_' + keyData.key + '" type="button"></button>';
    new dijit.form.Button({
        label: "#springMessage('regKey.delete')",
        onClick: function() {
            deleteKey(keyData.key);
        }
    }, "delete_" + keyData.key);
}

function removeRow(key) {
    var table = document.getElementById('keysTable');
    var rowCount = table.rows.length;
    try {
        for(var i=0; i<rowCount; i++) {
            var row = table.rows[i];
            var cell = row.cells[0];
            if(null != cell && cell.innerHTML == key) {
                table.deleteRow(i);
                rowCount--;
                i--;
            }

        }
    }catch(e) {
    }
}

function approveKey(id) {
    var button = dijit.byId("approve_" + id);
    button.set('disabled', true);
    dojo.xhrGet({
        url:"#springUrl('/secure/approveKey.json')?key=" + id,
        handleAs:"json",
        load: function(data){
            var res = data["result"];
            if (res == "NotFound") {
                button.set('label', '#springMessage("notFound.caption")');
            } else if (res == "AccessDenied") {
                button.set('label', '#springMessage("accessDenied.caption")');
            } else {
                button.set('label', '#springMessage("approved.caption")');
            }
        }
    });

}

function deleteKey(id) {
    var button = dijit.byId("delete_" + id);
    button.set('disabled', true);
    dojo.xhrGet({
        url:"#springUrl('/secure/deleteKey.json')?key=" + id,
        handleAs:"json",
        load: function(data){
            var res = data["result"];
            if (res == "NotFound") {
                button.set('label', '#springMessage("notFound.caption")');
            } else if (res == "AccessDenied") {
                button.set('label', '#springMessage("accessDenied.caption")');
            } else {
                removeRow(id);
                //button.set('label', '#springMessage("approved.caption")');
            }
        }
    });

}

function editCert(certId) {
    var form = document.forms.editCertForm;
    form.elements.id.value = certId;
    dijit.byId("saveCertButton").set('disabled', false);
    dijit.byId("cancelCertButton").set('disabled', false);
    dojo.xhrGet({
        url:"#springUrl('/secure/viewCertificate.json')?id=" + certId,
        handleAs:"json",
        load: function(data){
            dijit.byId("editUsrBtn").set('label', data.userData.name);
            for(var i in data){
                if (i == "userId")
                    form.userId.value = data[i];
                var elem =dijit.byId("cert" + i);
                if (elem != null ) {
                    if (data[i] != null)
                        elem.setValue(data[i]);
                    else
                        elem.setValue("");
                }

            }
            certDlg = dijit.byId("certDlg");
            certDlg.show();
        }
    });
}

function saveCert() {
    var form = document.forms.editCertForm;
    for(var i = 0; i < form.elements.length; i++) {
        var field = form.elements[i];
        if (field.name != "id" && field.name != "userId" && field.type == "hidden") {
            field.value = dijit.byId("cert" + field.name).getValue();
        }
    }
    dijit.byId("saveCertButton").set('disabled', true);
    dijit.byId("cancelCertButton").set('disabled', true);
    var json = dojo.formToJson("editCertForm");
    dojo.xhrPut({
        url: "#springUrl('/secure/saveCert.json')",
        handleAs: "json",
        putData: json,
        headers: { "Content-Type": "application/json"},
        load: function(data, ioArgs){
            certDlg.hide();
        },
        error: function(err, ioArgs){
            console.error(err); // display the error
            //dojo.byId(componentName).style.backgroundColor = "#FF0000";
        }
    });

}

function editUser(userId) {
    var form = document.forms.editUserForm;
    form.elements.id.value = userId;
    dijit.byId("saveUserButton").set('disabled', false);
    dijit.byId("cancelUserButton").set('disabled', false);
    dojo.xhrGet({
        url:"#springUrl('/secure/viewUser.json')?id=" + userId,
        handleAs:"json",
        load: function(data){
            for(var i in data){
                var elem =dijit.byId("user" + i);
                if (elem != null ) {
                    if (data[i] != null)
                        elem.setValue(data[i]);
                    else
                        elem.setValue("");

                }

            }
            userDlgKeys = dijit.byId("userDlgKeys");
            userDlgKeys.show();
        }
    });
}

function saveUser() {
    var form = document.forms.editUserForm;
    for(var i = 0; i < form.elements.length; i++) {
        var field = form.elements[i];
        if (field.name != "id" && field.type == "hidden") {
            field.value = dijit.byId("user" + field.name).getValue();
        }
    }
    var json = dojo.formToJson("editUserForm");
    dijit.byId("saveUserButton").set('disabled', true);
    dijit.byId("cancelUserButton").set('disabled', true);
    dojo.xhrPut({
        url: "#springUrl('/secure/saveUser.json')",
        putData: json,
        handleAs: "json",
        headers: { "Content-Type": "application/json"},
        load: function(data, ioArgs){
            if (data.result == "OK") {
                dijit.byId("editUsrBtn").set('label', form.name.value);
                userDlgKeys.hide();

            } else {

            }
        },
        error: function(err, ioArgs){
            console.error(err); // display the error
            //dojo.byId(componentName).style.backgroundColor = "#FF0000";
        }
    });

}


/////////////////
// userList
/////////////////

function updateUser(userId, fieldName, fieldValue) {
    var componentName = fieldName + "_" + userId;
    var form = dojo.byId("updateUserForm");
    for (var i = 0; i < form.elements.length; i++) {
        form.elements[i].value = form.elements[i].name == fieldName ? fieldValue : null;
    }
    dojo.byId("updateUserForm").id.value = userId;
    dojo.byId(componentName).style.backgroundColor = "#0000FF";

    dojo.xhrPost({
        url: "#springUrl('/secure/updateUser.json')",
        handleAs: "json",
        content: {
            userId: userId,
            fieldName: fieldName,
            fieldValue: fieldValue},
        load: function(data, ioArgs){
            if (data.result == "OK") {
                dojo.byId(componentName).style.backgroundColor = "#FFFFFF";
            } else {
                dojo.byId(componentName).style.backgroundColor = "#FF0000";
            }
        },
        error: function(err, ioArgs){
            console.error(err); // display the error
            //dojo.byId(componentName).style.backgroundColor = "#FF0000";
        }
    });
}

function deleteUser(userId, button) {
    button.set('disabled', true);
    dojo.xhrPost({
        url: "#springUrl('/secure/deleteUser.json')",
        content: {id: userId},
        load: function(data, ioArgs) {
            button.destroy(false);
        },
        error: function(err, ioArgs){
            console.error(err); // display the error
        }
    });
}
