import {Component, Input, OnInit} from '@angular/core';
import {Certificate} from '../certificates/certificates.component';
import {CabinetService} from '../cabinet.service';
import {BsModalRef} from 'ngx-bootstrap';

@Component({
  selector: 'app-certs-modal',
  templateUrl: './certs-modal.component.html',
  styleUrls: ['./certs-modal.component.css']
})
export class CertsModalComponent implements OnInit {

  public title: string;
  @Input() public certificates: Certificate[];

  constructor(public bsModalRef: BsModalRef, private cabinetService: CabinetService) {
    this.certificates = [];
  }

  ngOnInit() {
  }

}
