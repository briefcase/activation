import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CertsModalComponent } from './certs-modal.component';

describe('CertsModalComponent', () => {
  let component: CertsModalComponent;
  let fixture: ComponentFixture<CertsModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CertsModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CertsModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
