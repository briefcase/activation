import { Component, OnInit } from '@angular/core';

export class Organization {
  id: number;
  inn: string;
  orgName: string;
  region: string;
  country: string;
  city: string;
}

@Component({
  selector: 'app-organization',
  templateUrl: './organization.component.html',
  styleUrls: ['./organization.component.css']
})
export class OrganizationComponent implements OnInit {

  organization: Organization;
  constructor(private _organization: Organization) {
    this.organization = _organization;
  }

  ngOnInit() {
  }

}
