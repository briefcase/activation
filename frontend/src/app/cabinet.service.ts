import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import {Organization} from './organization/organization.component';
import {Observable} from 'rxjs/Observable';

import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/retry';
import {Key} from './keys/keys.component';
import {Certificate} from './certificates/certificates.component';
import {User} from './users/users.component';

@Injectable()
export class CabinetService {

  constructor(private http: Http) { }

  getOrganizations(size, skip, orgName, onNext: (organizations: Organization[]) => void) {
    const url = '/cabinet/secure/orgListPost.json'; // ?count=' + size + '&start=' + skip;
    // if (orgName) {
    //   url += '&org_name=' + orgName;
    // }
    this.http.post(url, {count: size, start: skip, orgName: orgName})
      .map(response => <Organization[]>response.json())
      .retry(2)
      .subscribe(onNext, error => {
        console.error(error);
      });
  }

  getKeys(size, skip, keyValue, organization, onNext: (keys: Key[]) => void) {
    const url = '/cabinet/secure/keyListPost.json';
    this.http.post(url, {count: size, start: skip, key: keyValue, orgId: organization ? organization.id : null})
      .map(response => <Key[]>response.json())
      .retry(2)
      .subscribe(onNext, error => {
        console.error(error);
      });
  }

  getCerts(size, skip, keyValue, organization, user, onNext: (certs: Certificate[]) => void) {
    const url = '/cabinet/secure/certListPost.json';
    this.http.post(url, {count: size, start: skip, key: keyValue, orgId: organization ? organization.id : null,
        userId: user ? user.id : null})
      .map(response => <Certificate[]>response.json())
      .retry(2)
      .subscribe(onNext, error => {
        console.error(error);
      });
  }
  getUsers(size, skip, name, mac, keyValue, organization, onNext: (users: User[]) => void) {
    const url = '/cabinet/secure/userListPost.json';
    this.http.post(url, {count: size, start: skip, name: name, macAddress: mac, key: keyValue,
          orgId: organization ? organization.id : null})
      .map(response => <User[]>response.json())
      .retry(2)
      .subscribe(onNext, error => {
        console.error(error);
      });
  }

  postOrganization(organization: Organization, onNext: (result: Organization) => void) {
    this.http.post('/cabinet/secure/saveOrg.json', organization)
      .map(response => <Organization>response.json())
      .retry(2).subscribe(onNext, error => {
        console.error(error);
    });
  }

  generateKeys(organization: Organization, keysCount: number, onNext: (result: Key[]) => void) {
    this.http.post('/cabinet/secure/generateKeys.json', {orgId: organization.id, count: keysCount})
      .map(response => <Key[]>response.json())
      .retry(2).subscribe(onNext, error => {
      console.error(error);
    });
  }
}
