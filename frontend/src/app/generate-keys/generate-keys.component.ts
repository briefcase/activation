import { Component, OnInit } from '@angular/core';
import {CabinetService} from '../cabinet.service';
import {BsModalRef} from 'ngx-bootstrap';
import {Organization} from '../organization/organization.component';
import { CompleterService, CompleterData } from 'ng2-completer';

@Component({
  selector: 'app-generate-keys',
  templateUrl: './generate-keys.component.html',
  styleUrls: ['./generate-keys.component.css']
})
export class GenerateKeysComponent implements OnInit {

  organizations: Organization[];
  keysCount: number;
  count: number;
  skip: number;
  orgName: string;
  public organization: Organization;
  public title: string;

  constructor(public bsModalRef: BsModalRef, private cabinetService: CabinetService) {
    this.organizations = [];
    this.organization = new Organization();
    this.keysCount = 1;
    this.count = 20;
    this.skip = 0;
    this.orgName = '';
  }

  ngOnInit() {
    this.cabinetService.getOrganizations(this.count, 0, this.orgName, organizations2 => {
      this.organizations = organizations2;
      this.skip = organizations2.length;
    });
  }

  onScroll() {
    this.cabinetService.getOrganizations(this.count, this.count, this.orgName, organizations2 => {
      this.organizations = this.organizations.concat(organizations2);
      this.skip += organizations2.length;
    });
  }

  search() {
    this.skip = 0;
    this.organizations = [];
    this.ngOnInit();
  }

  submit() {
    this.cabinetService.generateKeys(this.organization, this.keysCount, keys => {
      //
    });
  }
}
