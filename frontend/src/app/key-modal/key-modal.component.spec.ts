import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KeyModalComponent } from './key-modal.component';

describe('KeyModalComponent', () => {
  let component: KeyModalComponent;
  let fixture: ComponentFixture<KeyModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KeyModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KeyModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
