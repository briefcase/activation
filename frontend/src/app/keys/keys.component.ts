import {AfterViewInit, Component, ElementRef, OnInit, Renderer2, ViewChild} from '@angular/core';
import {Organization} from '../organization/organization.component';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {CabinetService} from '../cabinet.service';
import {KeyModalComponent} from '../key-modal/key-modal.component';
import {OrgService} from '../org.service';
import {CertsModalComponent} from '../certs-modal/certs-modal.component';
import {GenerateKeysComponent} from '../generate-keys/generate-keys.component';

export class Key {
  key: string;
  validDays: number;
  certificateId: number;
  orgName: string;
  macAddress: string;
  version: string;
  approved: boolean;
  orgId: number;
  organization: Organization;
}

@Component({
  selector: 'app-keys',
  templateUrl: './keys.component.html',
  styleUrls: ['./keys.component.css']
})
export class KeysComponent implements OnInit, AfterViewInit {

  keys: Key[];
  private curSkip: number;
  private size: number;
  keyValue: string;
  public useOrg = false;
  @ViewChild('scroller') scroller: ElementRef;
  bsModalRef: BsModalRef;
  organization: Organization;

  constructor(private cabinetService: CabinetService, private orgService: OrgService, private rd: Renderer2,
              private modalService: BsModalService) {
    this.curSkip = 0;
    this.size = 20;
    this.keyValue = '';
    orgService.setOrganization.subscribe(
      (val: boolean) => {
        this.useOrg = val;
        this.keys = [];
        this.curSkip = 0;
        this.size = 20;
        this.keyValue = '';
        this.ngOnInit();
      }
    );
  }

  ngOnInit() {
    this.organization = null;
    if (this.useOrg && this.orgService.getOrganization) {
      this.organization = this.orgService.getOrganization();
    }
    this.cabinetService.getKeys(this.size, this.curSkip, this.keyValue, this.organization, result => {
      this.keys = result;
      this.curSkip += result.length;
    });
  }

  ngAfterViewInit() {
    const height = window.innerHeight;
    if (height > 114) {
      this.rd.setStyle(this.scroller.nativeElement, 'height', height - 114);
      // this.scroller.nativeElement.css('min-height', height - 114);
    }
  }
  onScroll() {
    this.organization = null;
    if (this.useOrg && this.orgService.getOrganization) {
      this.organization = this.orgService.getOrganization();
    }
    this.cabinetService.getKeys(this.size, this.curSkip, this.keyValue, this.organization, result => {
      this.keys = this.keys.concat(result);
      this.curSkip += result.length;
    });
  }
  showCerts($event, key: Key) {
    this.bsModalRef = this.modalService.show(CertsModalComponent, {class: 'modal-dialog modal-lg'});
    this.bsModalRef.content.title = 'Сертификат';
    this.cabinetService.getCerts(100, 0, key.key, null, null, certs => {
      this.bsModalRef.content.certificates = certs;
    });
  }

  search() {
    this.keys = [];
    this.curSkip = 0;
    this.organization = null;
    this.ngOnInit();
  }

  generateKeys() {
    this.bsModalRef = this.modalService.show(GenerateKeysComponent);
    this.bsModalRef.content.title = 'Сертификат';
  }

  clearOrganization() {
    this.orgService.setOrganization.next(false);
    this.ngOnInit();
  }

}
