import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowKeysComponent } from './show-keys.component';

describe('ShowKeysComponent', () => {
  let component: ShowKeysComponent;
  let fixture: ComponentFixture<ShowKeysComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowKeysComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowKeysComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
