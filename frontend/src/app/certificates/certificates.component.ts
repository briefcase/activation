import {AfterViewInit, Component, ElementRef, OnInit, Renderer2, ViewChild} from '@angular/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {Organization} from '../organization/organization.component';
import {Key} from '../keys/keys.component';
import {CabinetService} from '../cabinet.service';
import {OrgService} from '../org.service';
import {User} from '../users/users.component';

export class Certificate {

  email: string;
  macAddress: string;
  issueDate: Date;
  endDate: Date;
  regKey: string;
  userData: User;
  userId: number;
  data: number[];
  id: number;
  inactive: boolean;
  orgName: string;
  version: string;
}

@Component({
  selector: 'app-certificates',
  templateUrl: './certificates.component.html',
  styleUrls: ['./certificates.component.css']
})
export class CertificatesComponent implements OnInit, AfterViewInit {

  certificates: Certificate[];
  private curSkip: number;
  private size: number;
  keyValue: string;
  public useOrg = false;
  public useKey = false;
  public useUser = false;
  @ViewChild('scroller') scroller: ElementRef;
  bsModalRef: BsModalRef;
  organization: Organization;
  user: User;
  key: Key;

  constructor(private cabinetService: CabinetService, private orgService: OrgService, private rd: Renderer2,
              private modalService: BsModalService) {
    this.curSkip = 0;
    this.size = 20;
    this.keyValue = '';
    orgService.setOrganization.subscribe(
      (val: boolean) => {
        this.useOrg = val;
        this.certificates = [];
        this.curSkip = 0;
        this.size = 20;
        this.keyValue = '';
        this.ngOnInit();
      }
    );
    orgService.setKey.subscribe((val: boolean) => {
      this.useKey = val;
      this.certificates = [];
      this.curSkip = 0;
      this.size = 20;
      this.keyValue = this.orgService.getKey ? this.orgService.getKey() : '';
      this.ngOnInit();
    });
    orgService.setUser.subscribe((val: boolean) => {
      this.useUser = val;
      this.certificates = [];
      this.curSkip = 0;
      this.size = 20;
      this.keyValue = '';
      this.ngOnInit();
    });
  }

  ngOnInit() {
    this.organization = null;
    if (this.useOrg && this.orgService.getOrganization) {
      this.organization = this.orgService.getOrganization();
    }
    this.key = null;
    if (this.useKey && this.orgService.getKey) {
      this.key = this.orgService.getKey();
      if (this.key) {
        this.keyValue = this.key.key;
      }
    }
    this.user = null;
    if (this.useUser && this.orgService.getUser) {
      this.user = this.orgService.getUser();
    }
    this.cabinetService.getCerts(this.size, this.curSkip, this.keyValue, this.organization, this.user, result => {
      this.certificates = result;
      this.curSkip += result.length;
    });
  }

  ngAfterViewInit() {
    const height = window.innerHeight;
    if (height > 114) {
      this.rd.setStyle(this.scroller.nativeElement, 'height', height - 120);
      // this.scroller.nativeElement.css('min-height', height - 114);
    }
  }
  onScroll() {
    this.organization = null;
    if (this.useOrg && this.orgService.getOrganization) {
      this.organization = this.orgService.getOrganization();
    }
    this.key = null;
    if (this.useKey && this.orgService.getKey) {
      this.key = this.orgService.getKey();
      if (this.key) {
        this.keyValue = this.key.key;
      }
    }
    this.user = null;
    if (this.useUser && this.orgService.getUser) {
      this.user = this.orgService.getUser();
    }
    this.cabinetService.getCerts(this.size, this.curSkip, this.keyValue, this.organization, this.user, result => {
      this.certificates = this.certificates.concat(result);
      this.curSkip += result.length;
    });
  }

  search() {
    this.certificates = [];
    this.certificates = [];
    this.curSkip = 0;
    this.organization = null;
    this.ngOnInit();
  }

  showUseOrg() {
    window.alert(this.useOrg);
  }

  clearOrganization() {
    this.orgService.setOrganization.next(false);
    this.ngOnInit();
  }
  clearUser() {
    this.orgService.setUser.next(false);
    this.ngOnInit();
  }
  clearKey() {
    this.orgService.setKey.next(false);
    this.ngOnInit();
  }
}
