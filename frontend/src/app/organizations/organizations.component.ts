import {AfterViewInit, Component, OnInit, TemplateRef, ViewChild, ViewContainerRef} from '@angular/core';
import {CabinetService} from '../cabinet.service';
import {Organization} from '../organization/organization.component';
import {ElementRef, Renderer2} from '@angular/core';
import {OrganizationModalComponent} from '../organization-modal/organization-modal.component';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';
import {OrgService} from '../org.service';

@Component({
  selector: 'app-organizations',
  templateUrl: './organizations.component.html',
  styleUrls: ['./organizations.component.css'],
})
export class OrganizationsComponent implements OnInit, AfterViewInit {

  organizations: Organization[];
  private curSkip: number;
  private size: number;
  private modalSubscribed: boolean;
  private editMode: boolean;
  orgName: string;
  @ViewChild('scroller') scroller: ElementRef;
  bsModalRef: BsModalRef;

  constructor(private cabinetService: CabinetService, private orgService: OrgService, private rd: Renderer2,
              private modalService: BsModalService) {
    this.curSkip = 0;
    this.size = 20;
    this.orgName = '';
    this.orgService.setModalResult.subscribe( organizationResult => {
      if (this.modalSubscribed) {
        if (this.editMode) {
          for (let i = 0; i < this.organizations.length; i++) {
            if (this.organizations[i].id == organizationResult.object.id) {
              this.organizations[i] = organizationResult.object;
            }
          }
        } else {
          this.organizations.splice(0, 0, organizationResult.object);
        }
        this.modalSubscribed = false;
      }
    });
  }

  ngOnInit() {
    this.curSkip = 0;
    this.size = 20;
    this.cabinetService.getOrganizations(this.size, this.curSkip, this.orgName, result => {
      this.organizations = result;
      this.curSkip += result.length;
    });
  }
  ngAfterViewInit() {
    const height = window.innerHeight;
    if (height > 120) {
      this.rd.setStyle(this.scroller.nativeElement, 'height', height - 120);
      // this.scroller.nativeElement.css('min-height', height - 114);
    }
  }
  onScroll() {
    this.cabinetService.getOrganizations(this.size, this.curSkip, this.orgName, result => {
      for (let i = 0; i < result.length; i++) {
        let add = true;
        for (let j = 0; j < this.organizations.length; j++) {
          if (this.organizations[j].id === result[i].id) {
            add = false;
            break;
          }
        }
        if (add) {
          this.curSkip++;
          this.organizations.push(result[i]);
        }
      }
      // this.organizations = this.organizations.concat(result);
      // this.curSkip += result.length;
    });
  }
  editOrganization($event, organization: Organization) {
    this.bsModalRef = this.modalService.show(OrganizationModalComponent);
    this.bsModalRef.content.title = 'Редактирование организации';
    this.bsModalRef.content.organization = Object.assign({}, organization);
    this.editMode = true;
    this.modalSubscribed = true;
  }
  addOrganization($event) {
    this.bsModalRef = this.modalService.show(OrganizationModalComponent);
    this.bsModalRef.content.title = 'Создание организации';
    this.bsModalRef.content.organization = new Organization();
    this.editMode = false;
    this.modalSubscribed = true;
  }

  search() {
    this.organizations = [];
    this.curSkip = 0;
    this.ngOnInit();
  }
  showKeys($event, organization: Organization) {
    this.orgService.getOrganization = function() {
      return organization;
    };
    this.orgService.setOrganization.next(true);
    this.orgService.showTab.next(1);
  }
  showCerts($event, organization: Organization) {
    this.orgService.getOrganization = function() {
      return organization;
    };
    this.orgService.setOrganization.next(true);
    this.orgService.showTab.next(2);
  }
  showUsers($event, organization: Organization) {
    this.orgService.getOrganization = function() {
      return organization;
    };
    this.orgService.setOrganization.next(true);
    this.orgService.showTab.next(3);
  }

}

