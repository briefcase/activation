import {Component, Input, OnInit} from '@angular/core';
import {BsModalRef} from 'ngx-bootstrap';
import {Organization} from '../organization/organization.component';
import {CabinetService} from '../cabinet.service';
import {OrgService} from '../org.service';

@Component({
  selector: 'app-organization-modal',
  templateUrl: './organization-modal.component.html',
  styleUrls: ['./organization-modal.component.css']
})
export class OrganizationModalComponent implements OnInit {

  public title: string;
  @Input() public organization: Organization;

  constructor(public bsModalRef: BsModalRef, private cabinetService: CabinetService, private orgService: OrgService) {
    this.organization = new Organization();
  }

  ngOnInit() {
  }

  onSubmit() {
    this.cabinetService.postOrganization(this.organization, organization => {
      this.orgService.setModalResult.next(organization);
      this.bsModalRef.hide();
    });
  }

}
