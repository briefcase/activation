import {Component, OnInit, ViewChild} from '@angular/core';
import {CabinetService} from './cabinet.service';
import {OrgService} from './org.service';
import {TabsetComponent} from 'ngx-bootstrap';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [CabinetService]
})
export class AppComponent implements OnInit {
  title = 'Центр управления лицензиями';

  @ViewChild('staticTabs') staticTabs: TabsetComponent;

  constructor(private orgService: OrgService) {
    orgService.showTab.subscribe((val: number) => {
      if (val) {
        this.staticTabs.tabs[val].active = true;
      }
    });
  }
  ngOnInit() {
  }

  initKeys() {

  }
}
