import { Injectable } from '@angular/core';
import {Subject} from 'rxjs/Subject';

@Injectable()
export class OrgService {

  constructor() { }

  getOrganization: Function;
  getUser: Function;
  getKey: Function;

  setUser: Subject<boolean> = new Subject();
  setKey: Subject<boolean> = new Subject();
  setOrganization: Subject<boolean> = new Subject();

  showTab: Subject<number> = new Subject();

  setModalResult: Subject<any> = new Subject();

}
