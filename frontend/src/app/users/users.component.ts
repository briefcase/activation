import {AfterViewInit, Component, ElementRef, OnInit, Renderer2, ViewChild} from '@angular/core';
import {Organization} from '../organization/organization.component';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {CabinetService} from '../cabinet.service';
import {OrgService} from '../org.service';
import {Key} from '../keys/keys.component';

export  class User {
  id: number;
  macAddress: string;
  inn: string;
  name: string;
  email: string;
  orgName: string;
  orgTitle: string;
  region: string;
  country: string;
  city: string;
  orgId: number;
  organization: Organization;

}

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit, AfterViewInit {

  users: User[];
  private curSkip: number;
  private size: number;
  name: string;
  mac: string;
  keyValue: string;
  public useOrg = false;
  public useKey = false;
  organization: Organization;
  key: Key;

  @ViewChild('scroller') scroller: ElementRef;
  bsModalRef: BsModalRef;

  constructor(private cabinetService: CabinetService, private orgService: OrgService, private rd: Renderer2,
              private modalService: BsModalService) {
    this.curSkip = 0;
    this.size = 20;
    this.name = '';
    this.mac = '';
    orgService.setOrganization.subscribe(
      (val: boolean) => {
        this.useOrg = val;
        this.users = [];
        this.curSkip = 0;
        this.size = 20;
        this.keyValue = '';
        this.ngOnInit();
      }
    );
    orgService.setKey.subscribe((val: boolean) => {
      this.useOrg = val;
      this.users = [];
      this.curSkip = 0;
      this.size = 20;
      this.keyValue = '';
      this.ngOnInit();
    });
  }

  ngOnInit() {
    this.curSkip = 0;
    this.size = 20;
    this.organization = null;
    if (this.useOrg && this.orgService.getOrganization) {
      this.organization = this.orgService.getOrganization();
    }
    this.key = null;
    if (this.useKey && this.orgService.getKey) {
      this.key = this.orgService.getKey();
      if (this.key) {
        this.keyValue = this.key.key;
      }
    }
    this.cabinetService.getUsers(this.size, this.curSkip, this.name, this.mac, this.keyValue, this.organization, result => {
      this.users = result;
      this.curSkip += result.length;
    });
  }
  ngAfterViewInit() {
    const height = window.innerHeight;
    if (height > 120) {
      this.rd.setStyle(this.scroller.nativeElement, 'height', height - 120);
    }
  }
  onScroll() {
    this.organization = null;
    if (this.useOrg && this.orgService.getOrganization) {
      this.organization = this.orgService.getOrganization();
    }
    this.key = null;
    if (this.useKey && this.orgService.getKey) {
      this.key = this.orgService.getKey();
      if (this.key) {
        this.keyValue = this.key.key;
      }
    }
    this.cabinetService.getUsers(this.size, this.curSkip, this.name, this.mac, this.keyValue, this.organization, result => {
      this.users = this.users.concat(result);
      this.curSkip += result.length;
    });
  }
  search() {
    this.users = [];
    this.curSkip = 0;
    this.ngOnInit();
  }
  showCerts($event, user: User) {
    this.orgService.getUser = function() {
      return user;
    };
    this.orgService.setUser.next(true);
    this.orgService.showTab.next(2);
  }
  clearOrganization() {
    this.orgService.setOrganization.next(false);
    this.ngOnInit();
  }
  clearKey() {
    this.orgService.setKey.next(false);
    this.ngOnInit();
  }

}
