import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { OrganizationComponent } from './organization/organization.component';
import { HttpModule} from '@angular/http';
import {HttpClientModule} from '@angular/common/http';
import { InfiniteScrollModule } from 'angular2-infinite-scroll';
import { AlertModule } from 'ngx-bootstrap';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { OrganizationsComponent } from './organizations/organizations.component';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { ModalModule } from 'ngx-bootstrap';
import { OrganizationModalComponent } from './organization-modal/organization-modal.component';
import { FormsModule } from '@angular/forms';
import {CabinetService} from './cabinet.service';
import { KeysComponent } from './keys/keys.component';
import { KeyModalComponent } from './key-modal/key-modal.component';
import {OrgService} from './org.service';
import { CertificateComponent } from './certificate/certificate.component';
import { CertificatesComponent } from './certificates/certificates.component';
import { UsersComponent } from './users/users.component';
import { CertsModalComponent } from './certs-modal/certs-modal.component';
import { GenerateKeysComponent } from './generate-keys/generate-keys.component';
import { ShowKeysComponent } from './show-keys/show-keys.component';
import {Ng2CompleterModule} from 'ng2-completer';


@NgModule({
  declarations: [
    AppComponent,
    OrganizationComponent,
    OrganizationsComponent,
    OrganizationModalComponent,
    KeysComponent,
    KeyModalComponent,
    CertificateComponent,
    CertificatesComponent,
    UsersComponent,
    CertsModalComponent,
    GenerateKeysComponent,
    ShowKeysComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    AlertModule.forRoot(),
    TabsModule.forRoot(),
    AccordionModule.forRoot(),
    ButtonsModule.forRoot(),
    ModalModule.forRoot(),
    InfiniteScrollModule,
    FormsModule,
    Ng2CompleterModule
  ],
  providers: [CabinetService, OrgService],
  bootstrap: [AppComponent],
  entryComponents: [OrganizationModalComponent, CertsModalComponent]
})
export class AppModule { }
