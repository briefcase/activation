# --------------------------------------------------------
# Host:                         127.0.0.1
# Server version:               5.5.18
# Server OS:                    Win32
# HeidiSQL version:             6.0.0.3603
# Date/time:                    2011-12-09 00:18:00
# --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

# Dumping structure for table cabinet.certs
DROP TABLE IF EXISTS `certs`;
CREATE TABLE IF NOT EXISTS `certs` (
  `_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `version` varchar(10) DEFAULT NULL,
  `inactive` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `mac` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `principal_name` varchar(150) DEFAULT NULL,
  `org_name` varchar(250) DEFAULT NULL,
  `issue_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `reg_key` char(21) DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `data` blob,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id` (`_id`),
  KEY `mac` (`mac`),
  KEY `email` (`email`),
  KEY `issue_date` (`issue_date`),
  KEY `reg_key` (`reg_key`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

# Data exporting was unselected.


# Dumping structure for table cabinet.reg_keys
DROP TABLE IF EXISTS `reg_keys`;
CREATE TABLE IF NOT EXISTS `reg_keys` (
  `key_value` char(21) NOT NULL,
  `valid_days` int(6) unsigned NOT NULL DEFAULT '14',
  `cert_id` int(10) unsigned NOT NULL DEFAULT '0',
  `principal_name` varchar(150) DEFAULT NULL,
  `org_name` varchar(250) DEFAULT NULL,
  `mac` varchar(50) DEFAULT NULL,
  `approved` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`key_value`),
  UNIQUE KEY `key_value` (`key_value`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

# Data exporting was unselected.


# Dumping structure for table cabinet.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `org_name` varchar(250) DEFAULT NULL,
  `org_title` varchar(250) DEFAULT NULL,
  `mac` varchar(50) DEFAULT NULL,
  `principal_name` varchar(150) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `inn` char(11) DEFAULT NULL,
  `change_mac` tinyint(1) DEFAULT NULL,
  `region` varchar(250) DEFAULT NULL,
  `country` char(2) DEFAULT NULL,
  `name` varchar(250) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id` (`_id`),
  KEY `mac` (`mac`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

# Data exporting was unselected.
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
