CREATE TABLE `certs` (
	`_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`inactive` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
	`mac` VARCHAR(50) NULL DEFAULT NULL,
	`email` VARCHAR(100) NULL DEFAULT NULL,
	`issue_date` DATETIME NULL DEFAULT NULL,
	`end_date` DATETIME NULL DEFAULT NULL,
	`reg_key` CHAR(21) NULL DEFAULT NULL,
	`user_id` INT(10) UNSIGNED NULL DEFAULT NULL,
	`data` BLOB NULL,
	PRIMARY KEY (`_id`),
	UNIQUE INDEX `_id` (`_id`),
	INDEX `mac` (`mac`),
	INDEX `email` (`email`),
	INDEX `issue_date` (`issue_date`),
	INDEX `reg_key` (`reg_key`),
	INDEX `user_id` (`user_id`)
)
COLLATE='utf8_general_ci'
ENGINE=MyISAM
ROW_FORMAT=DEFAULT
AUTO_INCREMENT=3