CREATE TABLE `reg_keys` (
	`key_value` CHAR(21) NOT NULL,
	`valid_days` INT(6) UNSIGNED NOT NULL DEFAULT '14',
	`cert_id` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	PRIMARY KEY (`key_value`),
	UNIQUE INDEX `key_value` (`key_value`)
)
COLLATE='utf8_general_ci'
ENGINE=MyISAM
ROW_FORMAT=DEFAULT